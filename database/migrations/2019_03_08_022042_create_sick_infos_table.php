<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSickInfosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('sick_infos'))
        {
            Schema::create('sick_infos', function (Blueprint $table) {
                $table->increments('id');
                $table->String('user_id',20)->nullable();
                $table->String('ic',20)->nullable();
                $table->String('disease_type',20)->nullable();
                $table->longtext('remark')->nullable();
                $table->integer('id_cif')->nullable();
                $table->tinyInteger('status')->nullable();
                $table->timestamps();
                $table->softDeletes();

            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sick_infos');
    }
}
