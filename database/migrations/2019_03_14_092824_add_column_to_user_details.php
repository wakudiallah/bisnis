<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnToUserDetails extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_details', function($table) {
            $table->String('Kodlokaliti',50)->nullable();
            $table->String('NamaLokaliti',150)->nullable();
            $table->String('NamaParlimen',150)->nullable();
            $table->String('NamaDM',150)->nullable();
            $table->String('NamaDun',150)->nullable();
            $table->String('Negeri',150)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_details', function (Blueprint $table) {
            //
        });
    }
}
