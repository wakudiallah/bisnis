<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAddressesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('addresses'))
        {
            Schema::create('addresses', function (Blueprint $table) {
                $table->increments('id');
                $table->String('add_type',10)->nullable();
                $table->String('ic',20)->nullable();
                $table->String('ref_no',20)->nullable();
                $table->String('address_1',200)->nullable();
                $table->String('address_2',200)->nullable();
                $table->String('address_3',200)->nullable();
                $table->String('postcode',10)->nullable();
                $table->String('city',150)->nullable();
                $table->String('state',150)->nullable();
                $table->String('desc',50)->nullable();
                $table->tinyInteger('status')->nullable();
                $table->timestamps();
                $table->softDeletes();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('addresses');
    }
}
