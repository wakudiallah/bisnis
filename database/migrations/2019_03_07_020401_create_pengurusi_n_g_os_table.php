<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePengurusiNGOsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('pengurusi_ngos'))
        {
            Schema::create('pengurusi_ngos', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('ngo_id')->nullable();
                $table->String('position',20)->nullable();
                $table->String('ic',20)->nullable();
                $table->String('name',100)->nullable();
                $table->String('phone',20)->nullable();
                $table->String('Kodlokaliti',50)->nullable();
                $table->String('NamaLokaliti',150)->nullable();
                $table->String('NamaParlimen',150)->nullable();
                $table->String('NamaDM',150)->nullable();
                $table->String('NamaDun',150)->nullable();
                $table->tinyInteger('status')->nullable();
                $table->tinyInteger('petugas_id')->nullable();
                $table->timestamps();
                $table->softDeletes();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pengurusi_ngos');
    }
}
