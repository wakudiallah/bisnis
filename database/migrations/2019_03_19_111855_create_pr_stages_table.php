<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePrStagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('pr_stages'))
        {
            Schema::create('pr_stages', function (Blueprint $table) {
                $table->increments('id');
                $table->String('code',20)->nullable();
                $table->String('desc',50)->nullable();
                $table->String('label',50)->nullable();
                $table->integer('user_id')->nullable();
                $table->tinyInteger('status')->nullable();
                $table->timestamps();
                $table->softDeletes();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pr_stages');
    }
}
