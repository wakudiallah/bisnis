<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnToTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('cifs', function($table) {
            $table->String('marital_status',10)->nullable();
            $table->String('occupation_id',10)->nullable();
            $table->String('no_dependants',10)->nullable();
            $table->String('school_dep',10)->nullable();
            $table->String('working_dep',10)->nullable();
            $table->String('parents_dep',10)->nullable();
            $table->String('unemployee_dep',10)->nullable();
            $table->String('beneficiary',10)->nullable();
            $table->longtext('remark_beneficiary')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cifs', function (Blueprint $table) {
            //
        });
    }
}
