<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDetailReportComplaintRepliesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('detail_report_complaint_replies'))
        {
            Schema::create('detail_report_complaint_replies', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('detail_report_complaint_id')->nullable();
                $table->String('remark',400)->nullable();
                $table->integer('user_id')->nullable();
                $table->integer('organization_id')->nullable();
                $table->string('status',30)->nullable();
                $table->timestamps();
                $table->softDeletes();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('detail_report_complaint_replies');
    }
}
