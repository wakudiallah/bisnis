<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnToDetailReportComplaintReplies extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('detail_report_complaint_replies'))
        {
            Schema::table('detail_report_complaint_replies', function (Blueprint $table) {
                $table->String('stage',50)->nullable();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('detail_report_complaint_replies', function (Blueprint $table) {
            //
        });
    }
}
