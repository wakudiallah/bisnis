<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePrHistoryComplaintsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('pr_history_complaints'))
        {
            Schema::create('pr_history_complaints', function (Blueprint $table) {
                $table->increments('id');
                $table->string('id_history',100)->nullable();
                $table->string('desc',100)->nullable();
                $table->string('color',20)->nullable();
                $table->integer('user_id')->nullable();
                $table->timestamps();
                $table->softDeletes();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pr_history_complaints');
    }
}
