<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePartiGabungansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        
        if(!Schema::hasTable('pr_parti_gabungans'))
        {
            Schema::create('pr_parti_gabungans', function (Blueprint $table) {
                $table->increments('id');
                $table->String('parti_code',20)->nullable();
                $table->String('parti_gabungan_desc',100)->nullable();
                $table->String('parti_id',20)->nullable();
                $table->String('picture',100)->nullable();
                $table->integer('user_id')->nullable();
                $table->tinyInteger('status')->nullable();
                $table->timestamps();
                $table->softDeletes();
            });
        }
    
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pr_parti_gabungans');
    }
}


