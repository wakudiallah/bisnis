<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class DetailReportComplaint extends Model
{
     protected $table 	= 'detail_report_complaints';

    public function user_rakyat()
    {
        return $this->belongsTo('App\Model\UserDetail','user_id','user_id');	
    }

     public function user_petugas()
    {
        return $this->belongsTo('App\User','petugas_id','id');	
    }

    public function pr_report()
    {
        return $this->belongsTo('App\Model\Parameter\PrReportComplaint','pr_report_complaints_id','id');	
    }

    public function reply()
    {
        return $this->hasone('App\Model\DetailReportComplaintReply', 'id', 'reply_id');
    }
    public function Address()
    {
        return $this->belongsTo('App\Model\Address','ref_no', 'ref_no');  
    }

    
}
