<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;


class CIF extends Model
{
    protected $table 	= 'cifs';


    public function petugas()
    {
        return $this->belongsTo('App\User','user_id','id');	
    }

     public function Address()
    {
        return $this->belongsTo('App\Model\Address','ref_no', 'ref_no');  
    }
}
