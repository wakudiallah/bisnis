<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class DetailImageReportComplaint extends Model
{
    protected $table 	= 'detail_image_report_complaints';
    
    public function image_report()
    {
        return $this->belongsTo('App\Model\DetailReportComplaint', 'detail_report_complaints_id', 'id');
    }
}
