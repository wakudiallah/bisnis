<?php

namespace App\Model\Parameter;

use Illuminate\Database\Eloquent\Model;

class PrBeneficiary extends Model
{
    protected $table = 'pr_beneficiaries';
}
