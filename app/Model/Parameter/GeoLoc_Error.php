<?php

namespace App\Model\Parameter;

use Illuminate\Database\Eloquent\Model;

class GeoLoc_Error extends Model
{
     protected $table = 'geolog_errors';
}
