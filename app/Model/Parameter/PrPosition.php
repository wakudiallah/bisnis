<?php

namespace App\Model\Parameter;

use Illuminate\Database\Eloquent\Model;

class PrPosition extends Model
{
    protected $table = 'pr_positions';
}
