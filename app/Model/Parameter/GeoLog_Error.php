<?php

namespace App\Model\Parameter;

use Illuminate\Database\Eloquent\Model;

class GeoLog_Error extends Model
{
     protected $table = 'geoloc_errors';
}
