<?php

namespace App\Model\Parameter;

use Illuminate\Database\Eloquent\Model;

class Postcode extends Model
{
    protected $table = 'postcode2';


	public function state() {
		return $this->belongsTo('App\Model\Parameter\State','state_code','state_code');	
		//Postcode dipunyai city
	}
}
