<?php

namespace App\Model\Parameter;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;

class Dun extends Model
{
    protected $table = 'param_duns';

    use SoftDeletes;

	protected $guarded = ["id"]; 
	protected $dates   = ['deleted_at'];
	public $timestamps = true;


	public function parlimen()
    {
        return $this->belongsTo('App\Model\Parameter\Parlimen','parlimen_code', 'code');  
    }
}
