<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class DetailReportComplaintReply extends Model
{
    protected $table = 'detail_report_complaint_replies';

    public function Stages()
    {
        return $this->hasone('App\Model\Parameter\PrStage', 'code', 'stage');
    }

     public function Org()
    {
        return $this->hasone('App\Model\Parameter\PrOrganization', 'id', 'organization_id');
    }

}
