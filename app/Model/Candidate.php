<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;

class Candidate extends Model
{
    
	protected $table = 'candidates';

    use SoftDeletes;

	protected $guarded = ["id"]; 
	protected $dates   = ['deleted_at'];
	public $timestamps = true;

	public function partigabungan()
    {
        return $this->belongsTo('App\Model\Parameter\PartiGabungan','parti_id', 'parti_code');  
    }
}
