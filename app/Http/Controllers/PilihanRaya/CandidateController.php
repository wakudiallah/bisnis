<?php

namespace App\Http\Controllers\PilihanRaya;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Session;
use Auth;
use PDF;
use File;
use App\Model\Parameter\Parlimen;
use App\Model\Parameter\Dun;
use App\Model\Parameter\State;
use App\Model\Parameter\Parti;
use App\Model\Parameter\PartiGabungan;
use App\Model\Parameter\PrOccupation;
use App\Model\Prnegeri;
use App\Model\Prk;
use App\Model\Survey;
use App\Model\Candidate;

class CandidateController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    

    public function create($id, $type)
    {
        if($type == 'prn'){

            $occupation = PrOccupation::where('status', '1')->get();
            $parti = PartiGabungan::where('status', '1')->get();

            return view('admin.pilihan_raya.prnegeri.add_maklumat_calon',compact('id', 'parti', 'type', 'occupation'));
        }
        elseif($type == 'prk'){

            $occupation = PrOccupation::where('status', '1')->get();
            $parti = PartiGabungan::where('status', '1')->get();

            return view('admin.pilihan_raya.prk.maklumat_calon_add',compact('id', 'parti', 'type', 'occupation'));
        }

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store($id, $type, Request $request)
    {
        $user_id   = Auth::user()->id;
        
        
        $born_date  = $request->input('born_date');
        $date =  date('Y-m-d 00:00:01', strtotime($born_date)); 
        


            $lokasi = $id;
            $tipe = $type;

            $file = $request->file('picture');
            $filename = $file->getClientOriginalName();
            $extension = $file->getClientOriginalExtension();
            $picture = $id.' - '.$filename;
            $destinationPath = base_path().'/public/admin/calon/'.$lokasi.'/';
            $file->move($destinationPath, $picture);

            $upload_file = $picture;


        $data              =  new Candidate;
            
        $data->picture     = $upload_file;
        $data->user_id     =  $user_id;
        $data->ic          =  $request->ic;
        $data->real_name   =  $request->fullname;
        $data->name        =  $request->name;
        $data->born_date   =  $date;
        $data->activity    =  $request->activity;
        $data->position    =  $request->position;
        $data->web         =  $request->web;
        $data->soc_fb      =  $request->fb;
        $data->soc_ig      =  $request->ig;
        $data->soc_twitter =  $request->twitter;
        $data->address     =  $request->address;
        $data->postcode    =  $request->postcode;
        $data->city        =  $request->city;
        $data->state    =  $request->state;
        $data->status   =  $request->status;
        $data->parti_id =  $request->parti;
        $data->phone1   =  $request->phone1;
        $data->phone2   =  $request->phone2;
        $data->telp     =  $request->telp;
        $data->email    =  $request->email;
        $data->status      =  '1';
        
        if($type           == 'prn'){
        $data->parlimen_id =  $id; 
        }elseif($type      == 'prk'){
        $data->dun_id      =  $id;  
        }

        $data->save();
         

         return redirect('/administrator/pilihan-raya/add-maklumat-calon/'.$id.'/'.$type)->with(['success' => 'Data Calon berjaya disimpan']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id, $type)
    {
        if($type == 'prn'){

            $parlimen  = Prnegeri::where('parlimen_code', $id)->first();
            $candidate = Candidate::where('parlimen_id', $id)->where('status', '1')->get();
            

            return view('admin.pilihan_raya.prnegeri.senarai_maklumat_calon',compact('id', 'parti', 'type','candidate','parlimen'));
        }
        elseif($type == 'prk'){
            $parlimen  = Prk::where('dun_code', $id)->first();
            $candidate = Candidate::where('dun_id', $id)->where('status', '1')->get();
            

            return view('admin.pilihan_raya.prk.maklumat_calon_senarai',compact('id', 'parti', 'type','candidate','parlimen'));
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id, $type, $calon)
    {
        if($type == 'prn'){

            $occupation = PrOccupation::where('status', '1')->get();
            $parti = PartiGabungan::where('status', '1')->get();
            $candidate = Candidate::where('parlimen_id', $id)->where('id', $calon)->first();   

            return view('admin.pilihan_raya.prnegeri.maklumat_calon_edit',compact('id', 'parti', 'type','candidate','parlimen', 'occupation'));
        }
        elseif($type == 'prk'){

            $occupation = PrOccupation::where('status', '1')->get();
            $candidate = Candidate::where('dun_id', $id)->where('id', $calon)->first();  
            $parti = PartiGabungan::where('status', '1')->get();

            return view('admin.pilihan_raya.prk.maklumat_calon_edit',compact('id', 'parti', 'type','candidate','parlimen', 'occupation'));

        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id, $type, $calon)
    {
        $user_id   = Auth::user()->id;

        $born_date  = $request->input('born_date');
        $date =  date('Y-m-d 00:00:01', strtotime($born_date)); 


        $ic          =  $request->input('ic');
        $real_name   =  $request->input('fullname');
        $name        =  $request->input('name');
        $activity    =  $request->input('activity');
        $position    =  $request->input('position');
        $web         =  $request->input('web');
        $soc_fb      =  $request->input('fb');
        $soc_ig      =  $request->input('ig');
        $soc_twitter =  $request->input('twitter');
        $address     =  $request->input('address');
        $postcode    =  $request->input('postcode');
        $city        =  $request->input('city');
        $state       =  $request->input('state');
        $status      =  $request->input('status');
        $parti_id    =  $request->input('parti');


        if ($request->hasFile('picture')) {
            $file = $request->file('picture');
            $filename = $file->getClientOriginalName();
            $extension = $file->getClientOriginalExtension();
            $picture = $id.' - '.$filename;
            $destinationPath = base_path().'/public/admin/calon/'.$id.'/';
            $file->move($destinationPath, $picture);

            $upload_file = $picture; 

            Candidate::where('id', $calon)->update(array(

            'ic'          => $ic, 
            'real_name'   => $real_name, 
            'name'        => $name, 
            'born_date'   => $date,
            'activity'    => $activity,
            'position'    => $position,
            'web'         => $web,
            'soc_fb'      => $soc_fb,
            'soc_ig'      => $soc_ig,
            'soc_twitter' => $soc_twitter,
            'address'     => $address, 
            'postcode'    => $postcode,
            'city'        => $city,
            'state'       => $state,
            'status'      => '1' ,
            'picture'   => $upload_file,
            'user_id' => $user_id

            )); 
        }
        else{

            Candidate::where('id', $calon)->update(array(

            'ic'          => $ic, 
            'real_name'   => $real_name, 
            'name'        => $name, 
            'born_date'   => $date,
            'activity'    => $activity,
            'position'    => $position,
            'web'         => $web,
            'soc_fb'      => $soc_fb,
            'soc_ig'      => $soc_ig,
            'soc_twitter' => $soc_twitter,
            'address'     => $address, 
            'postcode'    => $postcode,
            'city'        => $city,
            'state'       => $state,
            'status'      => '1' ,
            'user_id' => $user_id

            )); 

        }
         
        

        return redirect('/administrator/pilihan-raya/senarai-maklumat-calon/'.$id.'/'.$type)->with(['success' => 'Data Calon berjaya dikemaskini']);
        



    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($calon)
    {
        $emp = Candidate::findOrFail($calon)->delete();   
                        
        return redirect('/administrator/pilihan-raya/senarai-maklumat-calon/'.$id.'/'.$type)->with(['success' => 'Data Calon berjaya dipadam']); 
    }
}
