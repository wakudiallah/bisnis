<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Session;
use Auth;
use App\User;
use App\Model\Parameter\PrReportComplaint;
use App\Model\DetailReportComplaint;
use App\Model\DetailImageReportComplaint;
use App\Model\HistoryComplaint;
use App\Model\UserDetail;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Ramsey\Uuid\Uuid;
use App\Model\Parameter\PrService;
use App\Model\Parameter\PrAddress;
use App\Model\Address;

class ComplaintController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
  public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        
        if (Auth::guest()) {
            
            return redirect('/login');

        }else {

            $user = Auth::user()->user_id;
            $name = Auth::user()->name;
            $pr = PrReportComplaint::where('status', '1')->get(); 
            $ic =  UserDetail::where('user_id', $user)->first();
            $pr_service  = PrService::where('status',1)->get();

            return view('main.complaint', compact('pr', 'ic', 'name','pr_service')); 
        }
            
    }

     public function add($id)
    {   
        $user = Auth::user()->user_id;
        $name = Auth::user()->name;
        $pr = PrReportComplaint::where('status', '1')->get(); 
        $ic =  UserDetail::where('user_id', $user)->first();
        $pr_service  = PrService::where('status',1)->get();

        $report_complaint_id = $id;

        return view('main.complaint', compact('pr', 'ic', 'name','pr_service','report_complaint_id')); 
    }

    
    public function list()
    {       
      $user_id   = Auth::user()->user_id;
      $report = DetailReportComplaint::where('user_id', $user_id)->get();
       $report2 = DetailReportComplaint::where('user_id', $user_id)->get();
        return view('main.list', compact('report','report2'));  
    }


    public function save_report_complaints(Request $request)
    {
        $user_id   = Auth::user()->user_id;
        $name            = $request->input('name');
        $ic              = $request->input('ic');
        $pr_complaint_id = $request->input('pr_complaint_id');
        $title           = $request->input('title');
        $desc            = $request->input('desc');
        $address_1       = $request->input('address_1');
        $address_2       = $request->input('address_2');
        $address_3       = $request->input('address_3');
        $postcode        = $request->input('postcode');
        $city            = $request->input('city');
        $state           = $request->input('state');
        $phone           = $request->input('phone');


        $data                          = new DetailReportComplaint; 
        $data->user_id                 = $user_id; 
        $data->ic                      = $request->ic;
        $data->pr_report_complaints_id = $request->pr_complaint_id;
        $data->pr_service_id           = $request->jenis;
        $data->title                   = $request->title;
        $data->desc                    = $request->desc;
        $data->lat                     = $request->latitude;
        $data->lng                     = $request->longitude;
        $data->location                = $request->location;
        $data->ip                      = $request->ip;
        $data->save();

        $today          = date('Y-m-d H:i:s');
        $d              = date('d');
        $m              = date('m');
        $y              = date('y');
        $h              = date('h');
        $i              = date('i');

        if($pr_complaint_id=='1'){
            $ref_no = $d.$m.$y.$h.$i.$data->id.'AD';
        }
        else if($pr_complaint_id=='2'){
            $ref_no = $d.$m.$y.$h.$i.$data->id.'CD';
        }
        else if($pr_complaint_id=='3'){
            $ref_no = $d.$m.$y.$h.$i.$data->id.'BT';
        }

        

        $pr = PrReportComplaint::where('status', '1')->where('id',$pr_complaint_id)->limit('1')->first();

        $pr_address      = PrAddress::where('desc',$pr->desc)->limit('1')->first();

        $add                   =  new Address;
        $add->ic               = $ic;
        $add->add_type         = $pr_address->code;
        $add->ref_no           = $ref_no;
        $add->address_1        = $address_1;
        $add->address_2        = $address_2;
        $add->address_3        = $address_3;
        $add->postcode         = $postcode;
        $add->state            = $state;
        $add->city             = $city;
        $add->status           =  '1';
        $add->save();

        $update_ref = DetailReportComplaint::where('id', $data->id)->update(array('ref_no' => $ref_no)); 

        $history                          = new HistoryComplaint; 
        $history->associate_id()->associate($data);
        $history->user_id                 = $user_id; 
        $history->history_id                      = 'NEW';
        $history->activity = 'New Complaint';
        $history->save();


        
        $i = 1;
        if ($request->hasFile('filename')) {
        $files = $request->file('filename');
        foreach($files as $file){
            $filename = $file->getClientOriginalName();
            $extension = $file->getClientOriginalExtension();
            $picture = $i.'_'.$filename;
            $destinationPath = base_path().'/public/admin/complaint/'.$name;
            $file->move($destinationPath, $picture);

            $upload_file = $picture;

            $image                  = new DetailImageReportComplaint;
            $image->image_report()->associate($data);
            $image->filename  = $upload_file;
            $image->save();
            $i++;

        }

               
        return redirect('/')->with('success', 'Data aduan berjaya disimpan'); 
        }
    }
    

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    
  


    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
