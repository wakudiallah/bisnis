<?php

namespace App\Http\Controllers\Parameter;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Session;
use Auth;
use App\Model\Parameter\Parti;
use App\Model\Parameter\PartiGabungan;


class PrPartiGabunganController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pr = PartiGabungan::where('status', '1')->get();

        return view('admin.parameter.pr_partigabungan.index',compact('pr', 'state'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $pr = Parti::where('status', '1')->get();        

        return view('admin.parameter.pr_partigabungan.add',compact('pr', 'state'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $user = Auth::User()->id;


        $count = Parti::where('parti_desc',$parti)->count();

        if($count==0){
            if(!empty($request->parti)){
                $sizegroup = array_map(null, $request->x, $request->parti); 
                foreach ($sizegroup as $val) 
                    {
                        $data                      = new PartiGabungan;
                        $data->parti_gabungan_desc = $request->input("partigabungan");
                        $data->user_id             = $user;
                        $data->status              = '1';
                        $data->parti_id            = $val[1];
                        $data->save();
   
                    }
            }

            return redirect('/administrator/parameter/add-pr-partigabungan')->with(['success' => 'Data telah tersimpan']);
        }else{

            return redirect('/administrator/parameter/list-pr-partigabungan')->with(['warning' => 'Data telah ada']);
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
