<?php

namespace App\Http\Controllers\Parameter;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Session;
use Auth;
use App\Model\Parameter\Parti;

class PrPartiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pr = Parti::where('status', '1')->get();

        return view('admin.parameter.pr_parti.index',compact('pr', 'state'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.parameter.pr_parti.add',compact('pr', 'state'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = Auth::User();

        $parti        = $request->input('parti');
        $user_id     = $user->id;

        $count = Parti::where('parti_desc',$parti)->count();
        if($count==0){
            $pr_position          = new Parti;
            $pr_position->parti_desc    = $parti;
            $pr_position->user_id = $user_id;
            $pr_position->status  = '1';
            $pr_position->save();

            return redirect('/administrator/parameter/add-pr-parti')->with(['success' => 'Data telah tersimpan']);
        }
        else{
            return redirect('/administrator/parameter/list-pr-parti')->with(['success' => 'Data telah ada']);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $pr  = Parti::where('id', $id)->first();

        return view('admin.parameter.pr_parti.edit',compact('pr'));
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $parti         = $request->input('parti');

        $count = Parti::where('parti_desc',$parti)->count();
        if($count==0){
           
            Parti::where('id', $id)->update(array('parti_desc' => $parti )); 

            return redirect('/administrator/parameter/add-pr-parti')->with(['success' => 'Data telah tersimpan']);
        }
        else{
            return redirect('/administrator/parameter/list-pr-parti')->with(['warning' => 'Data telah ada']);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        
        $emp = Parti::findOrFail($id)->delete();   
                        
        return redirect('administrator/parameter/list-pr-organization')->with('warning', 'Data parameter berjaya dipadam'); 
    }
}
