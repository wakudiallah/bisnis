<?php

namespace App\Http\Controllers\Parameter;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Model\Parameter\PrBeneficiary;
Use Auth;

class PrBeneficiaryController extends Controller
{
       public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $pr  = PrBeneficiary::orderBy('id', 'DESC')->get();

        return view('admin.parameter.pr_beneficiary.index',compact('pr', 'state'));
    }

    public function dashboard()
    {
        $pr  = PrBeneficiary::orderBy('id', 'DESC')->get();

        return view('main.information.list_beneficiary',compact('pr'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       

        return view('admin.parameter.pr_beneficiary.add',compact(''));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        

        $user_id             = Auth::user()->id;
        
        $data                = new PrBeneficiary; 
        $data->user_id       = $user_id; 
        $data->title         = $request->input('title');
        $data->desc          = $request->input('desc');
        $data->link          = $request->input('link');
        $data->status        = '1';
        $data->save();

        return redirect('/administrator/parameter/add-pr-beneficiary')->with('success', 'Data parameter berjaya disimpan'); 
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
