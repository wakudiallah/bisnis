<?php

namespace App\Http\Controllers\Parameter;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Session;
use Auth;
use PDF;
use App\Model\Parameter\Parlimen;
use App\Model\Parameter\Dun;
use App\Model\Parameter\State;

class PrDunController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $pr  = Dun::orderBy('id', "DESC")->get();

        return view('admin.parameter.param_dun.index',compact('pr', 'state'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $pr  = Parlimen::where('status', 1)->get();
        $state  = State::get();

        $dun  = Dun::latest('id')->first();

        return view('admin.parameter.param_dun.add',compact('pr', 'state', 'dun'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user_id             = Auth::user()->id;

        $kod_parlimen = $request->input('parlimen_name');
        $code_parlimen   = substr($kod_parlimen,0, 4); 
        
        $data                = new Dun; 
        $data->user_id       = $user_id; 
        $data->code          = $request->input('code_name');
        $data->dun_name      = $request->input('dun_name');
        $data->parlimen_code = $code_parlimen;
        $data->state_code    = $request->input('state_name');
        $data->status        = '1';
        $data->save();


        return redirect('/administrator/parameter/add-pr-dun')->with('success', 'Data parameter berjaya disimpan'); 
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $emp = Dun::findOrFail($id)->delete();   
                        
        return redirect('administrator/parameter/list-pr-dun')->with('success', 'Data parameter berjaya dipadam'); 
    }
}
