<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    //protected $redirectTo = '/administrator';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    
    protected function authenticated(Request $request, $user)
    {
        if (Auth::user()->role == '1'){
            return redirect('/administrator');
        }
        else if (Auth::user()->role == '3'){
            return redirect('/administrator');
        }
        else if (Auth::user()->role == '2'){
            return redirect('/')->with(['Success' => 'Login Success']);
        }else{

            return redirect('/');
            //return redirect('/')->with(['warning' => 'Wrong Email/IC or Password']);
        }

        
    }


    /*public function redirectPath() 
    {  
        if (Auth::user()->role_id == '1'){
            return '/administrator';
        }

        return '/';
    }*/
    


    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }
}
