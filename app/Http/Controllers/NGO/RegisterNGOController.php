<?php

namespace App\Http\Controllers\NGO;

use App\Model\PengurusiNGO;
use App\Model\RegisterNGO;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use Ramsey\Uuid\Uuid;
use Illuminate\Support\Facades\Redirect;
use App\Model\Parameter\PrPosition;
use App\Model\Parameter\PrAddress;
use App\Model\Address;

class RegisterNGOController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
     public function __construct()
    {
        $this->middleware('auth');
    }
 
    public function index()
    {

        $ngo = RegisterNGO::get();
        return view('admin.parliament.ngo.index',compact('ngo'));
    }

    public function add()
    {
        return view('admin.parliament.ngo.add',compact(''));
    }

     public function detail_ngo($id)
    {
        $ngo = RegisterNGO::where('ngo_reg_number',$id)->first();

        $ajk = PengurusiNGO::where('ngo_id',$ngo->id)->get();
        $position = PrPosition::get();

        return view('admin.parliament.ngo.detail',compact('ngo','ajk','position'));
    }

    public function save_detail(Request $request)
    {

        //$ngo_id     = $request->input('ngo_id');
        //$position     = $request->input('position');

        //$tim_peng = PengurusiNGO::where('ngo_id',$ngo_id)->where('position','1')->count();

        $data              =  new PengurusiNGO;
        $data->ngo_id      =  $request->ngo_id;
        $data->ic          =  $request->ic;
        $data->position    =  $request->position;
        $data->name        =  $request->name;
        $data->phone       =  $request->phone;
        $data->NamaDM      =  $request->NamaDM_ajk;
        $data->NamaDUN     =  $request->NamaDUN_ajk;
        $data->NamaParlimen=  $request->NamaParlimen_ajk;
        $data->save();

          return Redirect::back()->with(['success' => 'Data telah tersimpan']);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store(Request $request)
    {
        //
    }


    /**
     * Display the specified resource.
     *
     * @param  \App\RegisterNGO  $registerNGO
     * @return \Illuminate\Http\Response
     */
    public function show(RegisterNGO $registerNGO)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\RegisterNGO  $registerNGO
     * @return \Illuminate\Http\Response
     */
    public function edit(RegisterNGO $registerNGO)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\RegisterNGO  $registerNGO
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, RegisterNGO $registerNGO)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\RegisterNGO  $registerNGO
     * @return \Illuminate\Http\Response
     */
    public function destroy(RegisterNGO $registerNGO)
    {
        //
    }

     public function save_register(Request $request)
    {
        
        $ngo_name           = $request->input('ngo_name');
        $ngo_reg_number     = $request->input('ngo_reg_number');
        $phone_number       = $request->input('phone_number');
        $email              = $request->input('email');
        $total_membership   = $request->input('total_membership');
        $chairman_ic        = $request->input('chairman_ic');
        $chairman_name      = $request->input('chairman_name');
        $chairman_phone     = $request->input('chairman_phone');
        $NamaDM             = $request->input('NamaDM');
        $NamaDUN            = $request->input('NamaDUN');
        $NamaParlimen       = $request->input('NamaParlimen');
        $user               = Auth::User();
        //$user   = Auth::user()->id;
        $id     = Uuid::uuid4()->tostring();

        $check_ngo = RegisterNGO::where('ngo_reg_number',$ngo_reg_number)->count();
        $check_email = RegisterNGO::where('email',$email)->count();

        if(($check_ngo=='0') && ($check_email=='0')){

            $ngo                    = new RegisterNGO; 
            $ngo->user_id           = $id;    
            $ngo->ngo_name          = $ngo_name;    
            $ngo->ngo_reg_number    = $ngo_reg_number;
            $ngo->phone_number      = $phone_number;
            $ngo->email             = $email;
            $ngo->total_membership  = $total_membership;
            $ngo->chairman_ic       = $chairman_ic;
            $ngo->chairman_name     = $chairman_name;
            $ngo->chairman_phone    = $chairman_phone;
            $ngo->NamaDM            = $NamaDM;
            $ngo->NamaDun           = $NamaDUN;
            $ngo->NamaParlimen      = $NamaParlimen;
            $ngo->petugas_id        = $user->id;
            $ngo->save();    

            $today          = date('Y-m-d H:i:s');
            $d              = date('d');
            $m              = date('m');
            $y              = date('y');
            $h              = date('h');
            $i              = date('i');

            $ref_no = $d.$m.$y.$h.$i.$ngo->id.'NG';

            $pr_address      = PrAddress::where('desc','NGO')->limit('1')->first();

            $data                   =  new Address;
            $data->ic               = $chairman_ic;
            $data->add_type         = $pr_address->code;
            $data->ref_no           = $ref_no;
            $data->status           =  '1';
            $data->save();

            $update_ngo = RegisterNGO::where('id', $ngo->id)->update(array('ref_no' => $ref_no)); 

            return redirect('/administrator/ngo/detail_ngo/'.$ngo_reg_number)->with(['success' => 'Data NGO berjaya disimpan']);
        }
        else if (($check_ngo=='1') && ($check_email=='0'))
        {
            return redirect('/account-register')->with(['success' => 'Nombor Pendaftaran NGO telah berdaftar']);
        }
        else if (($check_ngo=='0') && ($check_email=='1'))
        {
            return redirect('/account-register')->with(['success' => 'Alamat Emel telah berdaftar']);
        }
        else if (($check_ngo=='1') && ($check_email=='1'))
        {
            return redirect('/account-register')->with(['success' => 'NGO telah berdaftar']);
        }
    }


     public function save(Request $request)
    {
        
        $ngo_name           = $request->input('ngo_name');
        $ngo_reg_number     = $request->input('ngo_reg_number');
        $phone_number       = $request->input('phone_number');
        $email              = $request->input('email');
        $total_membership   = $request->input('total_membership');
        $chairman_ic        = $request->input('chairman_ic');
        $chairman_name      = $request->input('chairman_name');
        $chairman_phone     = $request->input('chairman_phone');
        $NamaDM             = $request->input('NamaDM');
        $NamaDUN            = $request->input('NamaDUN');
        $NamaParlimen       = $request->input('NamaParlimen');
        $postcode           = $request->input('postcode');
        $address_1          = $request->input('address_1');
        $address_2          = $request->input('address_2');
        $address_3          = $request->input('address_3');
        $state              = $request->input('state');
        $city               = $request->input('city');
        $ref_no             = $request->input('ref_no');

        $user               = Auth::User();
        

        $update_ngo = RegisterNGO::where('ngo_reg_number', $ngo_reg_number)->update(array('phone_number' => $phone_number,'total_membership' => $total_membership,'chairman_ic' => $chairman_ic,'chairman_name' => $chairman_name,'NamaDM' => $NamaDM,'NamaDUN' => $NamaDUN,'NamaParlimen' => $NamaParlimen)); 

        $update_address = Address::where('ref_no', $ref_no)->update(array('address_1' => $address_1,'address_2' => $address_2,'address_3' => $address_3,'state' => $state,'city' => $city,'postcode' => $postcode)); 

        return redirect('/administrator/ngo/detail_ngo/'.$ngo_reg_number)->with(['success' => 'Data NGO berjaya diperbaharui']);
       
    }
}
