<?php

namespace App\Http\Controllers\Survey;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use Ramsey\Uuid\Uuid;
use Illuminate\Support\Facades\Redirect;
use App\Model\Survey;
use App\Model\CIF;
use DB;
class SurveyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
     public function __construct()
    {
        $this->middleware('auth');
    }
 
    public function index()
    {

        $survey = Survey::get();
        return view('admin.parliament.survey.index',compact('survey'));
    }

    public function add()
    {
        return view('admin.parliament.survey.add',compact(''));
    }

     public function detail_survey($ic)
    {
       $survey = Survey::where('ic',$ic)->limit('1')->first();
       
        return view('admin.parliament.survey.detail',compact('survey'));
    }

    public function save(Request $request)
    {

        $user_id   = Auth::user()->id;

        $data              		=  new Survey;
        $data->user_id      	=  $user_id;
        $data->ic          		=  $request->ic_survey;
        $data->name        		=  $request->name_survey;
        $data->phone       		=  $request->phone;
        $data->Kodlokaliti      =  $request->kodlokaliti_survey;
        $data->NamaLokaliti     =  $request->NamaLokaliti_survey;
        $data->NamaDM      		=  $request->NamaDM_survey;
        $data->NamaDUN     		=  $request->NamaDUN_survey;
        $data->NamaParlimen 	=  $request->NamaParlimen_survey;
        $data->Negeri           =  $request->Negeri_survey;
        $data->kecenderungan 	=  $request->kecenderungan;
        $data->status 			=  '1';
        $data->save();

         return redirect('/administrator/survey/index')->with(['success' => 'Data telah tersimpan']);
    }

     public function chart()
    {

        $user = Auth::User();
        

        $putih = DB::table('surveys')
        ->select( 'surveys.NamaDM', DB::raw('count(surveys.id) as total'))
        ->where('surveys.kecenderungan','1') 
         ->count();

          $hitam = DB::table('surveys')
        ->select( 'surveys.NamaDM', DB::raw('count(surveys.id) as total'))
        ->where('surveys.kecenderungan','3') 
         ->count();

         $kelabu = DB::table('surveys')
        ->select( 'surveys.NamaDM', DB::raw('count(surveys.id) as total'))
        ->where('surveys.kecenderungan','2') 
         ->count();




        return view('admin.parliament.survey.chart',compact('hitam','putih','kelabu'))
        ->with('hitam',json_encode($hitam, JSON_NUMERIC_CHECK))
        ->with('putih',json_encode($putih, JSON_NUMERIC_CHECK))
        ->with('kelabu',json_encode($kelabu, JSON_NUMERIC_CHECK));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store(Request $request)
    {
        //
    }


    /**
     * Display the specified resource.
     *
     * @param  \App\RegisterNGO  $registerNGO
     * @return \Illuminate\Http\Response
     */
    public function show(RegisterNGO $registerNGO)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\RegisterNGO  $registerNGO
     * @return \Illuminate\Http\Response
     */
    public function edit(RegisterNGO $registerNGO)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\RegisterNGO  $registerNGO
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, RegisterNGO $registerNGO)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\RegisterNGO  $registerNGO
     * @return \Illuminate\Http\Response
     */
    public function destroy(RegisterNGO $registerNGO)
    {
        //
    }

     public function save_register(Request $request)
    {
        
        $ngo_name           = $request->input('ngo_name');
        $ngo_reg_number     = $request->input('ngo_reg_number');
        $phone_number       = $request->input('phone_number');
        $email              = $request->input('email');
        $total_membership   = $request->input('total_membership');
        $chairman_ic        = $request->input('chairman_ic');
        $chairman_name      = $request->input('chairman_name');
        $chairman_phone     = $request->input('chairman_phone');
        $NamaDM             = $request->input('NamaDM');
        $NamaDUN            = $request->input('NamaDUN');
        $NamaParlimen       = $request->input('NamaParlimen');

        //$user   = Auth::user()->id;
        $id     = Uuid::uuid4()->tostring();

        $check_ngo = RegisterNGO::where('ngo_reg_number',$ngo_reg_number)->count();

        if($check_ngo=='0'){

            $ngo                    = new RegisterNGO; 
            $ngo->ngo_name          = $ngo_name;    
            $ngo->ngo_reg_number    = $ngo_reg_number;
            $ngo->phone_number      = $phone_number;
            $ngo->email             = $email;
            $ngo->total_membership  = $total_membership;
            $ngo->chairman_ic       = $chairman_ic;
            $ngo->chairman_name     = $chairman_name;
            $ngo->chairman_phone    = $chairman_phone;
            $ngo->NamaDM            = $NamaDM;
            $ngo->NamaDUN           = $NamaDUN;
            $ngo->NamaParlimen      = $NamaParlimen;
            $ngo->save();    


            return redirect('/administrator/ngo/detail_ngo/'.$ngo_reg_number)->with(['success' => 'Alamat Emel sudah terdaftar, sila cuba menggunakan emel lain']);
        }
        else{
            return redirect('/account-register')->with(['success' => 'NGO sudah terdaftar']);
        }
    }
}
