<?php

namespace App\Http\Controllers\Admin\ReportComplaint;

use App\Model\DetailImageReportComplaint;
use Illuminate\Http\Request;

class DetailImageReportComplaintController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\DetailImageReportComplaint  $detailImageReportComplaint
     * @return \Illuminate\Http\Response
     */
    public function show(DetailImageReportComplaint $detailImageReportComplaint)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Model\DetailImageReportComplaint  $detailImageReportComplaint
     * @return \Illuminate\Http\Response
     */
    public function edit(DetailImageReportComplaint $detailImageReportComplaint)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\DetailImageReportComplaint  $detailImageReportComplaint
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, DetailImageReportComplaint $detailImageReportComplaint)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\DetailImageReportComplaint  $detailImageReportComplaint
     * @return \Illuminate\Http\Response
     */
    public function destroy(DetailImageReportComplaint $detailImageReportComplaint)
    {
        //
    }
}
