<?php

namespace App\Http\Controllers\ReportComplaint;


use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Session;
use Auth;
use App\Model\Parameter\PrReportComplaint;

class PrReportComplaintController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $report  = PrReportComplaint::orderby('created_at','ASC')->get();
        return view('admin.parameter.pr_report_complaint.index',compact('report'));
    }

    public function add()
    {
        return view('admin.parameter.pr_report_complaint.add');
    }

     public function save(Request $request)
    {
        $user = Auth::User();

        $desc        = $request->input('desc');
        $user_id     = $user->id;

        $pr_report_complaint          = new PrReportComplaint;
        $pr_report_complaint->desc    = $desc;
        $pr_report_complaint->user_id = $user_id;
        $pr_report_complaint->status  = '1';
        $pr_report_complaint->save();

        return redirect('/administrator/parameter/add-pr-report-complaint')->with(['success' => 'Data telah tersimpan']);
        
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\Parameter\PrReportComplaint  $prReportComplaint
     * @return \Illuminate\Http\Response
     */
    public function show(PrReportComplaint $prReportComplaint)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Model\Parameter\PrReportComplaint  $prReportComplaint
     * @return \Illuminate\Http\Response
     */
    public function edit(PrReportComplaint $prReportComplaint)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\Parameter\PrReportComplaint  $prReportComplaint
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, PrReportComplaint $prReportComplaint)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\Parameter\PrReportComplaint  $prReportComplaint
     * @return \Illuminate\Http\Response
     */
    public function destroy(PrReportComplaint $prReportComplaint)
    {
        //
    }
}
