<?php

namespace App\Http\Controllers\Web;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Session;
use Auth;
use App\User;
use App\Model\Main\News;

class BeritaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $sd  = News::orderby('created_at','DESC')->get();
        return view('admin.web.news.index',compact('sd'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.web.news.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $user_id   = Auth::user()->id;
        $user_name   = Auth::user()->name;

        $file            = $request->file('picture');
        $filename        = $file->getClientOriginalName();
        $extension       = $file->getClientOriginalExtension();
        $picture         = $filename;
        $destinationPath = base_path().'/public/main/images/berita/';
        $file->move($destinationPath, $picture);

        $upload_file = $picture;


        $data            =  new News;
        
        $data->picture   =  $upload_file;
        $data->title     =  $request->title;
        $data->news      =  $request->news;
        $data->pic_title =  $request->title_pic;
        $data->author_id =  $user_id;
        $data->status    =  '0';
        $data->save();
         

        return redirect('/administrator/web/news/add')->with(['success' => 'Data Berita berjaya disimpan']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $sd  = News::where('id', $id)->first();
        return view('admin.web.news.edit',compact('sd'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user_id   = Auth::user()->id;
        $user_name   = Auth::user()->name;

         $title     =  $request->title;
        $news      =  $request->news;
        $pic_title =  $request->title_pic;

        if($request->hasFile('picture')){ 
            $file            = $request->file('picture');
            $filename        = $file->getClientOriginalName();
            $extension       = $file->getClientOriginalExtension();
            $picture         = $filename;
            $destinationPath = base_path().'/public/main/images/berita/';
            $file->move($destinationPath, $picture);

            $upload_file = $picture;


            News::where('id', $id)->update(array('picture' => $upload_file, 'title' => $title, 'news' => $news, 'pic_title' => $pic_title)); 
        }
        else{
            News::where('id', $id)->update(array( 'title' => $title, 'news' => $news, 'pic_title' => $pic_title));
        }

        return redirect('/administrator/web/news/index')->with(['success' => 'Data Berita berjaya dikemaskini']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $emp = News::findOrFail($id)->delete();   
                        
        return redirect('administrator/web/news/index')->with('success', 'Data Berita berjaya dipadam'); 
    }
}
