<?php

namespace App\Http\Controllers;

use App\Model\Role\Role;
use App\Model\Role\Permission;
use App\Model\Role\Post;
use Illuminate\Http\Request;

class RoleController extends Controller
{
    //use Authorizable;

    public function index()
    {
        $roles = Role::all();
        $permissions = Permission::all();

        return view('role.index', compact('roles', 'permissions'));
    }

    public function store(Request $request)
    {
        $this->validate($request, ['name' => 'required|unique:roles']);

        if( Role::create($request->only('name')) ) {
           
        }

        return redirect()->back();
    }

    public function update(Request $request, $id)
    {
        if($role = Role::findOrFail($id)) {
            // admin role has everything
            if($role->name === 'Admin') {
                $role->syncPermissions(Permission::all());
                return redirect()->route('roles.index');
            }

            $permissions = $request->get('permissions', []);
            $role->syncPermissions($permissions);
            ;
        } else {
            
        }

        return redirect()->route('roles.index');
    }

}
