<?php

namespace App\Http\Controllers\Organization;

use App\Model\PengerusiOrganization;
use App\Model\RegisterOrganization;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use Ramsey\Uuid\Uuid;
use Illuminate\Support\Facades\Redirect;
use App\Model\Parameter\PrPosition;
use App\Model\Parameter\PrAddress;
use App\Model\Address;

class PengerusiOrganizationController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
 
    public function index()
    {

        $organization = RegisterOrganization::get();
        return view('admin.parliament.organization.index',compact('organization'));
    }

    public function add()
    {
        return view('admin.parliament.organization.add',compact(''));
    }

     public function detail_organization($id)
    {
        $organization = RegisterOrganization::where('org_reg_number',$id)->first();

        $ajk = PengurusiOrganization::where('org_id',$org->id)->get();
        $position = PrPosition::get();

        return view('admin.parliament.organization.detail',compact('organization','ajk','position'));
    }

    public function save_detail(Request $request)
    {

        $data              =  new PengurusiOrganization;
        $data->org_id      =  $request->org_id;
        $data->ic          =  $request->ic;
        $data->position    =  $request->position;
        $data->name        =  $request->name;
        $data->phone       =  $request->phone;
        $data->NamaDM      =  $request->NamaDM_ajk;
        $data->NamaDUN     =  $request->NamaDUN_ajk;
        $data->NamaParlimen=  $request->NamaParlimen_ajk;
        $data->save();

          return Redirect::back()->with(['success' => 'Data telah tersimpan']);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store(Request $request)
    {
        //
    }


    /**
     * Display the specified resource.
     *
     * @param  \App\RegisterNGO  $registerNGO
     * @return \Illuminate\Http\Response
     */
    public function show(RegisterOrganization $registerOrganization)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\RegisterNGO  $registerNGO
     * @return \Illuminate\Http\Response
     */
    public function edit(RegisterOrganization $registerOrganization)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\RegisterNGO  $registerNGO
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, RegisterOrganization $registerOrganization)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\RegisterNGO  $registerNGO
     * @return \Illuminate\Http\Response
     */
    public function destroy(RegisterOrganization $registerOrganization)
    {
        //
    }

     public function save_register(Request $request)
    {
        
        $org_name           = $request->input('org_name');
        $org_reg_number     = $request->input('org_reg_number');
        $phone_number       = $request->input('phone_number');
        $email              = $request->input('email');
        $total_membership   = $request->input('total_membership');
        $chairman_ic        = $request->input('chairman_ic');
        $chairman_name      = $request->input('chairman_name');
        $chairman_phone     = $request->input('chairman_phone');
        $NamaDM             = $request->input('NamaDM');
        $NamaDUN            = $request->input('NamaDUN');
        $NamaParlimen       = $request->input('NamaParlimen');

        //$user   = Auth::user()->id;
        $id     = Uuid::uuid4()->tostring();

        $check_org = RegisterOrganization::where('org_reg_number',$org_reg_number)->count();

        if($check_org=='0'){

            $org                    = new RegisterOrganization; 
            $org->org_name          = $org_name;    
            $org->org_reg_number    = $org_reg_number;
            $org->phone_number      = $phone_number;
            $org->email             = $email;
            $org->total_membership  = $total_membership;
            $org->chairman_ic       = $chairman_ic;
            $org->chairman_name     = $chairman_name;
            $org->chairman_phone    = $chairman_phone;
            $org->NamaDM            = $NamaDM;
            $org->NamaDUN           = $NamaDUN;
            $org->NamaParlimen      = $NamaParlimen;
            $org->save();    


            return redirect('/administrator/organization/detail_organization/'.$org_reg_number)->with(['success' => 'Alamat Emel sudah terdaftar, sila cuba menggunakan emel lain']);
        }
        else{
            return redirect('/account-register')->with(['success' => 'Organization sudah terdaftar']);
        }
    }
}
