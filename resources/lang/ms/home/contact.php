<?php

return [
	'contact' => 'Hubungi Kami',
	'sentence' => 'Terdapat banyak cara untuk menghubungi kami. Anda boleh menjatuhkan kami garis, memberi kami panggilan atau menghantar e-mel, memilih yang paling sesuai dengan anda',
	'weekend' => 'Hujung minggu ditutup',
	'weekday'=>'Buka: 8.00-18.00 Isnin-Jumat',
	'get' => 'Semak untuk hubungi kami',
	'fill' => 'Isilah borang di bawah ini.',
	'name' => 'Nama',
	'email' => 'Emel',
	'message' => 'Mesej',
	'send' => 'Hantar Mesej'
];