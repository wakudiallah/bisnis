<?php

return [
	'register' => 'Daftar',
	'name' => 'Nama',
	'ic' => 'My Kad',
	'phone' => 'Telefon Bimbit',
	'address' => 'Alamat',
	'postcode' => 'Poskod',
	'city' => 'Bandar',
	'state' => 'Negeri',
	'email' => 'E-mel',
	'password' => 'Kata Laluan',
	'personal' => 'Individu',
	'party' => 'Ahli Parti',
	'as' => 'Daftar sebagai',
];