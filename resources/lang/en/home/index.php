<?php

return [
	'aduan' => 'Complaints',
	'service' => 'Services',
	'welcome' => 'WELCOME TO SMARAK',
	'welcome2' => 'Sistem Maklumat Rakyat - Malaysia',
	'day' => 'Day',
	'hour' => 'Hours',
	'min' => 'Mins',
	'sec' => 'Sec',
	'create' => 'Create New Complaints/Report',
	'list' => 'Check Complaints/Report List',
	'start' => 'Starts',
];