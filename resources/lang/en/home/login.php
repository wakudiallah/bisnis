<?php

return [
	'login' => 'Login',
	'password' => 'Password',
	'create' => 'If you have no member account, you can create account here.'
];