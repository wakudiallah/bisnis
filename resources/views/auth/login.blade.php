@extends('main.layout.template')

@section('content')

	

	<div class="blogs">
		<div class="container" >

			<form method="POST" action="{{ route('login') }}">
                @csrf
                <div class="row" style="padding-top: 60px !important; padding-bottom:  60px !important">
                    
                    <div class="col text-center">
                        <div class="section_title">
                            <h2>{{trans('home/login.login')}}</h2>
                        </div>
                    </div>
                    <div class="col-lg-4"></div>
                    <div class="col-lg-8">
                        <div>
                            <input id="email" class="form_input input_name input_ph{{ $errors->has('email') ? ' is-invalid' : '' }}"" type="email" name="email" placeholder="Email / IC" required="required" data-error="Name is required." required autofocus>
                            @if ($errors->has('email'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                            @endif

                                

							<input id="password" type="password" class="form_input input_email input_ph {{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" placeholder="{{trans('home/login.password')}}" required="required" data-error="Valid email is required.">

							@if ($errors->has('password'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                            @endif
						</div>

						<div>
							<button id="review_submit" type="submit" class="newsletter_submit_btn trans_300" value="Submit">{{trans('home/login.login')}}</button>
							<p><a href="{{url('account-register')}}">{{trans('home/login.create')}}</a></p>

						</div>
							
					</div>
				</div>
			</form>

			
		</div>
	</div>

	
	

@endsection

