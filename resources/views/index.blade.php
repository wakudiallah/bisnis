    @extends('main.layout.template_home')

    @section('content')


    <link rel="stylesheet" type="text/css" href="{{asset('main/styles/single_responsive.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('main/styles/single_styles.css')}}">


    <!-- Slider -->

    <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
      <ol class="carousel-indicators">
        <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
        <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
        <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
      </ol>
      <div class="carousel-inner">
        <div class="carousel-item active">
            <!--
            <h6>{{trans('home/index.welcome')}}</h6>
            <h1>{{trans('home/index.welcome2')}}</h1> -->
          <img src="{{url('main/images/slider4.jpg')}}" class="d-block w-100" alt="...">
        </div>

        <?php 
            $slider = \App\Model\Main\Slider::where('status','1')->orderby('created_at','DESC')->get();
        ?>

        @foreach($slider as $slider)
        <div class="carousel-item">
            <img src="{{url('main/images/'.$slider->picture)}}" class="d-block w-100" alt="...">
        </div>
        @endforeach
        
        
      </div>
      <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
      </a>
      <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
      </a>
    </div>



    <div class="blogs">
        <div class="container">
            <div class="row">
                <div class="col text-center">
                    <div style="margin-bottom: 80px" class="section_title new_arrivals_title">
                        <h2>Berita</h2>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-8">
                    <div class="single_product_pics">
                        <div class="row">
                            

                            @foreach($news as $data)
                            <div class="col-lg-6 image_col order-lg-2 order-1">
                                <div class="single_product_image">
                                    <?php 
                                            $tarikh =  date('d-m-Y ', strtotime($data->created_at)); 
                                            $waktu = date('h:i A', strtotime($data->created_at));
                                            $stringCut = substr($data->news, 0, 250);
                                    ?>

                                    <a href="{{url('/detail-berita/'.$data->id)}}">
                                        <img src="{{url('main/images/berita/'.$data->picture)}}" class="img-fluid" alt="..." >
                                        <h4>{{$data->title}}</h4>
                                        <p><i class="fa fa-calendar"></i> {{$tarikh}}<i class="fa fa-clock-o"></i> {{$waktu}}</p>
                                        <p>{{$stringCut}}...</p>
                                    </a>
                                </div>
                            </div>
                            @endforeach

                            
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="product_details">
                        <div class="product_details_title">
                            <ul class="list-unstyled">
                            
                            @foreach($other_news as $data1)
                            <a href="{{url('/detail-berita/'.$data1->id)}}">
                                <li class="media">
                                    <img src="{{url('main/images/berita/'.$data1->picture)}}" class="img-fluid" alt="..." width="20%" height="20%" style="margin-right: 10px"> 
                                    <div class="media-body">
                                      <h5 class="mt-0 mb-1"></h5>
                                      <p style="font-size: 10px"><i class="fa fa-calendar"></i> Senin, Maret 18 th, 2019 <i class="fa fa-clock-o"></i> 11:13 am <br>
                                       {{$data1->title}}</p>
                                    </div>
                                </li>
                            </a>
                            @endforeach
                                

                            </ul>

                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>



    <!-- Deal of the week -->

    <div class="new_arrivals">
        <div class="container">
            <div class="row">
                <div class="col text-center">
                    <div class="section_title new_arrivals_title">
                        <h2>Statistik</h2>
                    </div>
                </div>
            </div>
            <div class="row align-items-center" style="margin-top: 20px !important">
                <div class="col-lg-6">
                    <div class="">
                        <div id="piechart" style="width: 600px; height: 500px;"></div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="">
                        <div id="columnchart_material" style="width: 500px; height: 500px;"></div>
                    </div>
                </div>
               
            </div>
        </div>
    </div>

   
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript">
      google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(drawChart);

      function drawChart() {

        var data = google.visualization.arrayToDataTable([
          ['Task', 'Hours per Day'],
          ['Work',     11],
          ['Eat',      2],
          ['Commute',  2],
          ['Watch TV', 2],
          ['Sleep',    7]
        ]);

        var options = {
          title: 'My Daily Activities'
        };

        var chart = new google.visualization.PieChart(document.getElementById('piechart'));

        chart.draw(data, options);
      }
    </script>


    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript">
      google.charts.load('current', {'packages':['bar']});
      google.charts.setOnLoadCallback(drawChart);

      function drawChart() {
        var data = google.visualization.arrayToDataTable([
          ['Year', 'Sales', 'Expenses', 'Profit'],
          ['2014', 1000, 400, 200],
          ['2015', 1170, 460, 250],
          ['2016', 660, 1120, 300],
          ['2017', 1030, 540, 350]
        ]);

        var options = {
          chart: {
            title: 'Company Performance',
            
          }
        };

        var chart = new google.charts.Bar(document.getElementById('columnchart_material'));

        chart.draw(data, google.charts.Bar.convertOptions(options));
      }
    </script>
  


   <!-- Blogs 
    <div class="blogs">
        <div class="container">
            <div class="row">
                <div class="col text-center">
                    <div class="section_title">
                        <h2>{{trans('home/index.service')}}</h2>
                    </div>
                </div>
            </div>
            <div class="row blogs_container">
                <a href="{{url('complaint')}}">
                <div class="col blog_item_col">
                    <div class="blog_item">
                        <div class="blog_background" style="background-image:url({{url('main/images/aduan.jpg')}}"></div>
                        <div class="blog_content d-flex flex-column align-items-center justify-content-center text-center">
                            <h3 class="blog_title">{{trans('home/index.aduan')}}</h3>
                            <a class="blog_more" href="#"></a>
                        </div>
                    </div>
                </div>
                </a>
                
            </div>
        </div>
    </div>

   
    <div class="benefit">
        <div class="container" id="one">
            <div class="row benefit_row align-center">
                <div class="col-lg-2 benefit_col"></div>
                <div class="col-lg-4 benefit_col">
                    <a href="{{url('complaint')}}">
                        <div class="benefit_item d-flex flex-row align-items-center">
                            <div class="benefit_icon"><i class="fa fa-comments" aria-hidden="true"></i></div>
                            <div class="benefit_content">
                                <h6>{{trans('home/index.aduan')}}</h6>
                                <p>{{trans('home/index.create')}}</p>
                            </div>
                        </div>
                    </a>
                </div>
                
                <div class="col-lg-4 benefit_col">
                    <a href="{{url('complaints/list')}}">
                    <div class="benefit_item d-flex flex-row align-items-center">
                        <div class="benefit_icon"><i class="fa fa-list-alt" aria-hidden="true"></i></div>
                        <div class="benefit_content">
                            <h6>{{trans('home/index.list')}}</h6>
                            
                        </div>
                    </div>
                    </a>
                </div>
                <div class="col-lg-2 benefit_col"></div>
            </div>
        </div>
    </div>
    -->



    <script src="{{asset('main/jssingle_custom.js')}}"></script>

    @endsection