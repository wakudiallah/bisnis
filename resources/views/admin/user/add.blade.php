@extends('admin.layout.template_form')

@section('content_admin')


<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-4">
        <h2>Tambah Pengguna</h2>
        <ol class="breadcrumb">
            <li>
                <a href="index.html">Home</a>
            </li>
            <li>
                <a>Pengguna</a>
            </li>
            <li class="active">
                <strong>Tambah Pengguna</strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">
    </div>
</div>

<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Tambah Pengguna Baharu</h5>
                    <div class="ibox-tools"></div>
                        <div class="ibox-content">
                            <form class="form-horizontal" method="post" action="{{url('/administrator/user/save')}}">
                                {{csrf_field()}}
                                 <div class="form-group">
                                    <label class="col-sm-2 control-label"></label>
                                    <div class="col-md-3">
                                        <p class="font-bold">
                                            Nama Penuh
                                        </p>
                                        <input type="text" class="form-control" name="name" required="">
                                    </div>
                                    <div class="col-md-3">
                                        <p class="font-bold">
                                            Nombor Telepon Bimbit
                                        </p>
                                        <input type="text" class="form-control" name="phone" required="">
                                    </div>
                                    <div class="col-md-4">
                                        <p class="font-bold">
                                            Negeri
                                        </p>
                                         <select class="form-control m-b" name="role">
                                            @foreach($role as $role)
                                            <option value="{{$role->id}}">{{$role->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                 <div class="form-group">
                                    <label class="col-sm-2 control-label"></label>
                                    <div class="col-md-5">
                                        <p class="font-bold">
                                           E-mel
                                        </p>
                                        <input type="text" class="form-control" name="email" required="">
                                    </div>
                                    <div class="col-md-5">
                                        <p class="font-bold">
                                            Password
                                        </p>
                                        <input type="password" class="form-control" name="password" required="">
                                    </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                     <label class="col-sm-2 control-label"></label>
                                    <div class="col-md-3">
                                       
                                    </div>
                                    <div class="col-md-3"> 
                                        <div class="col-sm-4 col-sm-offset-2">
                                        <button class="btn btn-primary" type="submit">Simpan</button>
                                    </div>
                                    </div>
                                    <label class="col-sm-2 control-label"></label>
                                    <div class="col-md-3">
                                       
                                    </div>
                                </div>
                               <br><br>
                             
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


@endsection





