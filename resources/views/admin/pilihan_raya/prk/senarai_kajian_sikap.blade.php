@extends('admin.layout.template_datatables')

@section('content_admin')
<style>
    .button {
      background-color: #4CAF50; /* Green */
      border: none;
      color: white;
      padding: 10px 20px;
      text-align: center;
      text-decoration: none;
      display: inline-block;
      font-size: 16px;
      margin: 0px 0px;
      cursor: pointer;
    }

    .button2 {background-color: #008CBA;} /* Blue */
    .button3 {background-color: #f44336;} /* Red */ 
    .button4 {background-color: #e7e7e7; color: black;} /* Gray */ 
    .button5 {background-color: #555555;} /* Black */
</style>

    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>Senarai Kajian Sikap</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="{{url('/administrator')}}">Home</a>
                </li>
                <li>
                    <a href="{{url('/administrator/pilihan-raya/prk/index')}}">PRK</a>
                </li>
                <li class="active">
                    <strong>Senarai Kajian Sikap</strong>
                </li>
            </ol>
        </div>
        <div class="col-lg-2"></div>
    </div>



    <div class="wrapper wrapper-content animated fadeInRight">
        
        
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>SENARAI KAJIAN SIKAP <b style="color: red !important">{{$parlimen->parlimen_code}} - {{$parlimen->name_parlimen->parlimen_name}} : {{$parlimen->dun_code}} - {{$parlimen->name_dun->dun_name}}</b> </h5>
                        <div class="ibox-tools">
                          
                        </div>
                    </div>

                    <div class="ibox-content">
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover dataTables-example" >
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Nama</th>
                                        <th>IC</th>
                                        <th>Kecenderungan</th>
                                        <th>Nama DM</th>
                                        <th>Negeri</th>
                                        <th>Tarikh</th>
                                        <th>Petugas</th>
                                        <th class="align-center">History</th>
                                        <th>Tindakan</th>
                                    </tr>
                                </thead>
                              
                                <tbody>
                                <?php $i=1; ?>
                                @foreach($senarai as $survey)
                                    <tr class="gradeX">
                                        <td>{{$i}}</td>
                                        <td>{{$survey->name}}</td>
                                        <td>{{$survey->ic}}</td>
                                        @if($survey->kecenderungan=='p')
                                        <td>   <button class="button button3">Putih</button></td>
                                        @elseif($survey->kecenderungan=='h')
                                        <td> <button class="button button5">Hitam</button></td>
                                        @elseif($survey->kecenderungan=='k')
                                        <td> <button class="button button4">Kelabu</button></td>
                                        @endif
                                        <td>{{$survey->NamaDM}}</td>
                                        <td>{{$survey->Negeri}}</td>
                                        <td>{{$survey->created_at}}</td>
                                        <td>{{$survey->user_petugas->name}}</td>
                                        <td class="align-center">
                                            
                                            <a href="{{url('/')}}/administrator/pilihan-raya/prn/hitory/{{$parlimen->parlimen_code}}/{{$survey->ic}}" type="submit" class="btn btn-warning btn-circle btn-lg" onclick="window.open('{{url('/')}}/administrator/pilihan-raya/prn/hitory/{{$parlimen->parlimen_code}}/{{$survey->ic}}', 'newwindow', 'width=750,height=400'); return false;">
                                            <i class="fa fa-search"></i>
                                            </a>


                                            
                                        </td>
                                        <td class="center">
                                            <a href="{{url('/')}}/administrator/survey/detail/{{$survey->ic}}" type="submit" class="btn btn-danger" style="cursor:pointer;">
                                            <i class="fa fa-paste"></i> Detail
                                            </a>
                                        </td>
                                        
                                    </tr>
                                <?php  $i++; ?>
                                @endforeach
                                </tbody>
                                
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection