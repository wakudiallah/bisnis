<!DOCTYPE html>

<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">

        <title>SEMARAK</title>

        <link href="{{asset('admin/css/bootstrap.min.css')}}" rel="stylesheet">
        <link href="{{asset('admin/font-awesome/css/font-awesome.css')}}" rel="stylesheet">
        <link href="{{asset('admin/css/plugins/iCheck/custom.css')}}" rel="stylesheet">
        <link href="{{asset('admin/css/plugins/dataTables/datatables.min.css')}}" rel="stylesheet">
        <!-- Toastr style -->
        
        <link href="{{asset('admin/css/animate.css')}}" rel="stylesheet">
        <link href="{{asset('admin/css/style.css')}}" rel="stylesheet">

        <link rel="icon" href="{{asset('main/images/favicon.ico')}}" type="image/x-icon">
    </head>
    @include('admin.layout.admin_header')
    @yield('content_admin')
    @include('admin.layout.admin_footer')
        <!-- Mainly scripts -->
        <script src="{{asset('admin/js/jquery-3.1.1.min.js')}}"></script>
        <script src="{{asset('admin/js/bootstrap.min.js')}}"></script>
        <script src="{{asset('admin/js/plugins/metisMenu/jquery.metisMenu.js')}}"></script>
        <script src="{{asset('admin/js/plugins/slimscroll/jquery.slimscroll.min.js')}}"></script>
        <!-- Custom and plugin javascript -->
        <script src="{{asset('admin/js/inspinia.js')}}"></script>
        <script src="{{asset('admin/js/plugins/pace/pace.min.js')}}"></script>
        <!-- Steps -->
        <script src="{{asset('admin/js/plugins/dataTables/datatables.min.js')}}"></script>
        <script>
            $(document).ready(function(){
                $('.dataTables-example').DataTable({
                pageLength: 25,
                responsive: true,
                dom: '<"html5buttons"B>lTfgitp',
                buttons: [
                    //{ extend: 'copy'},
                    //{extend: 'csv'},
                    //{extend: 'excel', title: 'ExampleFile'},
                    {extend: 'pdf', title: 'PPBM - N24'},
                    {extend: 'print',
                    customize: function (win){
                        $(win.document.body).addClass('white-bg');
                        $(win.document.body).css('font-size', '10px');
                        $(win.document.body).find('table').addClass('compact').css('font-size', 'inherit');
                        }
                    }]
                });
            });
        </script>
         <script type="text/javascript">

    $(function () { 
        var data_asses = <?php echo $putihx; ?>;
        var data_sub = <?php echo $hitamx; ?>;
        var data_dis = <?php echo $kelabux; ?>; 


        Highcharts.chart('container', {
            chart: {
                type: 'column'
            },
            title: {
                text: 'Senarai Total Kecenderungan Berdasarkan DM'
            },
            subtitle: {
                text: 'DUN SEMENYIH'
            },

            xAxis: {
                categories: [
                'BANDAR RINCHING SEKSYEN 1 - 4',
                'BANDAR RINCHING SEKSYEN 5 - 6',
                'BANDAR TASIK KESUMA',
                'BERANANG',
                'BUKIT MAHKOTA',
                'HULU SEMENYIH',
                'KAMPUNG BAHARU SEMENYIH',
                'KAMPUNG BATU 26 BERANANG',
                'KAMPUNG RINCHING',
                'KAMPUNG TANJONG',
                'KANTAN PERMAI',
                'KUALA PAJAM',
                'PEKAN SEMENYIH',
                'PENJARA KAJANG',
                'SEMENYIH BARAT',
                'SEMENYIH INDAH',
                'SEMENYIH SELATAN',
                'SESAPAN BATU',
                'SESAPAN BATU REMBAU',
                'SESAPAN KELUBI',
                'SUNGAI JAI',
                'SUNGAI KEMBUNG',
                'TARUN'
                ],
                crosshair: true
            },
            yAxis: {
                min: 0,
                max: 250,
                tickInterval: 50,
                title: {
                    text: 'Total Data'
                }
            },
            tooltip: {
                headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                    '<td style="padding:0"><b>{point.y:.0f} data</b></td></tr>',
                footerFormat: '</table>',
                shared: true,
                useHTML: true
            },
            plotOptions: {
                column: {
                    pointPadding: 0.2,
                    borderWidth: 0
                }
            },



            series: [{
                name: 'Putih',
                
                data: data_asses
                

            }, {
                name: 'Kelabu',
                data: data_dis

            }, {
                name: 'Hitam',
                data: data_sub

            }],

            colors: ['#be0d12', '#808080','#0c0c0c']
        });

        });
</script>
    </body>
</html>

