<body>

    @include('sweetalert::alert')

    <div id="wrapper">

    <nav class="navbar-default navbar-static-side" role="navigation">
        <div class="sidebar-collapse">
            <ul class="nav metismenu" id="side-menu">
                <li class="nav-header">
                    <div class="dropdown profile-element"> <span>
                            <img alt="image" class="img-circle" src="{{asset('admin/img/profile_small.jpg')}}" />
                             </span>
                        <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                            <span class="clear"> <span class="block m-t-xs"> <strong class="font-bold">{{ Auth::user()->name }}</strong>

                             </span>

                                <span class="text-muted text-xs block">
                                @if(!empty(Auth::user()->role))
                                {{ Auth::user()->us->role }}
                                @endif 
                                <b class="caret"></b>
                                </span>
                            </span> </a>

                        <ul class="dropdown-menu animated fadeInRight m-t-xs">
                           <!-- <li><a href="profile.html">Profile</a></li>
                            <li><a href="contacts.html">Contacts</a></li>
                            <li><a href="mailbox.html">Mailbox</a></li>-->
                            <li class="divider"></li>
                            <li>  <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                        <i class="fa fa-sign-out"></i> Log out
                    </a></li>
                        </ul>
                    </div>
                    <div class="logo-element">
                        SEMARAK
                    </div>
                </li>

                <li class="{{ Request::is('administrator') ? 'active' : '' }}">
                    <a href="{{url('administrator')}}"><i class="fa fa-th-large"></i> <span class="nav-label">Dashboard</span>  </a>
                </li>
                <li>
                    <a href="#"><i class="fa fa-institution"></i> <span class="nav-label">Parlimen </span><span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level collapse">
                            <li>
                                 <a href="{{url('administrator')}}"><i class="fa fa-id-card-o"></i> <span class="fa arrow"></span>Maklumat Rakyat</span></a>
                                <ul class="nav nav-third-level">
                                    <li>
                                        <a href="{{url('administrator/information_public/index')}}"><i class="fa fa-search"></i> Carian</a>
                                    </li>
                                   <li>
                                        <a href="{{url('administrator/information_public/list')}}"><i class="fa fa-list-ul"></i>Senarai</a>
                                    </li>

                                </ul>
                            </li>
                            <!--<?php 
                                    $preport = \App\Model\Parameter\PrReportComplaint::where('status','1')->orderby('id','ASC')->get();
                            ?>
                            <li>
                                <a href="{{url('administrator')}}"><i class="fa fa-paste"></i> <span class="fa arrow"></span>Laporan Rakyat</span></a>
                                <ul class="nav nav-second-level collapse">
                                    @foreach($preport as $preport)
                                    <li>
                                        <a href="#">{{$preport->desc}} <span class="fa arrow"></span></a>
                                        <ul class="nav nav-third-level">
                                            <li>
                                                <a href=" {{url('/')}}/administrator/report-complaint/index/{{$preport->id}}"><i class="fa fa-list-ul"></i>Senarai</a>
                                            </li>
                                        </ul>
                                    </li>
                                    @endforeach
                                </ul>
                            </li>-->
                            <li>
                                 <a href="#"><i class="fa fa-id-card-o"></i> <span class="fa arrow"></span>Kajian Sikap</span></a>
                                <ul class="nav nav-third-level">
                                    <li>
                                        <a href="{{url('administrator/survey/add')}}"><i class="fa fa-search"></i> Carian</a>
                                    </li>
                                   <li>
                                        <a href="{{url('administrator/survey/index')}}"><i class="fa fa-list-ul"></i>Senarai</a>
                                    </li>
                                    <li>
                                        <a href="{{url('administrator/survey/chart')}}"><i class="fa fa-bar-chart-o"></i>Chart</a>
                                    </li>

                                </ul>
                            </li>
                        </ul>
                </li>
                 <li>
                    <a href="#"><i class="fa fa-archive"></i> <span class="nav-label">Pelayanan </span><span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level collapse">
                            <?php 
                                $preport = \App\Model\Parameter\PrReportComplaint::where('status','1')->orderby('id','ASC')->get();
                            ?>
                            @foreach($preport as $preport)
                            <li>
                                
                                <a href="#">{{$preport->desc}} <span class="fa arrow"></span></a>
                                        <ul class="nav nav-third-level">
                                            <li>
                                                <a href=" {{url('/')}}/administrator/report-complaint/index/{{$preport->id}}"><i class="fa fa-list-ul"></i>Senarai</a>
                                            </li>
                                        </ul>
                               
                            </li>
                             @endforeach
                        </ul>
                </li>
                <li>
                    <a href="#"><i class="fa fa-slideshare"></i> <span class="nav-label">NGO</span><span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level collapse">
                        <li>
                            <a href="{{url('administrator/ngo/add')}}"><i class="fa fa-list-ul"></i>Tambah</a>
                        </li>
                        <li>
                            <a href="{{url('administrator/ngo/index')}}"><i class="fa fa-table"></i>Senarai</a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="#"><i class="fa fa-check"></i> <span class="nav-label">Pilihan Raya </span><span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level collapse">
                            <li>
                                <a href="#">PRN <span class="fa arrow"></span></a>
                                <ul class="nav nav-third-level">
                                    <li>
                                        <a href="{{url('administrator/pilihan-raya/prn/add')}}"><i class="fa fa-plus"></i>Tambah</a>
                                    </li>
                                    <li>
                                        <a href="{{url('administrator/pilihan-raya/prn/index')}}"><i class="fa fa-table"></i>Senarai</a>
                                    </li>
                                </ul>
                            </li>
                            <li>
                                <a href="#">PRK<span class="fa arrow"></span></a>
                                <ul class="nav nav-third-level">
                                    <li>
                                        <a href="{{url('administrator/pilihan-raya/prk/add')}}"><i class="fa fa-plus"></i>Tambah</a>
                                    </li>
                                    <li>
                                        <a href="{{url('administrator/pilihan-raya/prk/index')}}"><i class="fa fa-table"></i>Senarai</a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                </li>
                

                <li>
                    <a href="#"><i class="fa fa-hospital-o"></i> <span class="nav-label">Organization</span><span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level collapse">
                        <li>
                            <a href="{{url('administrator/organization/add')}}"><i class="fa fa-list-ul"></i>Tambah</a>
                        </li>
                        <li>
                            <a href="{{url('administrator/organization/index')}}"><i class="fa fa-table"></i>Senarai</a>
                        </li>
                    </ul>
                </li>
                
                <li>
                    <a href="#"><i class="fa fa-user-o"></i> <span class="nav-label">Pengguna</span><span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level collapse">

                        <li class="{{ Request::is('administrator/user/index') ? 'active' : '' }}">
                            <a href="{{url('administrator/user/index')}}"><i class="fa fa-th-large"></i> <span class="nav-label">Daftar Pengguna</span></a>
                        </li>

                        <li class="{{ Request::is('administrator/user/add') ? 'active' : '' }}">
                            <a href="{{url('administrator/user/add')}}"><i class="fa fa-th-large"></i> <span class="nav-label">Tambah Pengguna</span></a>
                        </li>

                    </ul>
                </li>
                
                
                <li>
                    <a href="#"><i class="fa fa-sitemap"></i> <span class="nav-label">Parameter </span><span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level collapse">
                            <li>
                                <a href="#">PR Laporan Rakyat <span class="fa arrow"></span></a>
                                <ul class="nav nav-third-level">
                                    <li>
                                        <a href="{{url('administrator/parameter/add-pr-report-complaint')}}"><i class="fa fa-plus"></i>Tambah</a>
                                    </li>
                                    <li>
                                        <a href="{{url('administrator/parameter/list-pr-report-complaint')}}"><i class="fa fa-table"></i>Senarai</a>
                                    </li>
                                </ul>
                            </li>
                            <li>
                                <a href="#">PR Parti <span class="fa arrow"></span></a>
                                <ul class="nav nav-third-level">
                                    <li>
                                        <a href="{{url('administrator/parameter/add-pr-parti')}}"><i class="fa fa-plus"></i>Tambah</a>
                                    </li>
                                    <li>
                                        <a href="{{url('administrator/parameter/list-pr-parti')}}"><i class="fa fa-table"></i>Senarai</a>
                                    </li>
                                </ul>
                            </li>
                            <li>
                                <a href="#">PR Parti Gabungan <span class="fa arrow"></span></a>
                                <ul class="nav nav-third-level">
                                    <li>
                                        <a href="{{url('administrator/parameter/add-pr-partigabungan')}}"><i class="fa fa-plus"></i>Tambah</a>
                                    </li>
                                    <li>
                                        <a href="{{url('administrator/parameter/list-pr-partigabungan')}}"><i class="fa fa-table"></i>Senarai</a>
                                    </li>
                                </ul>
                            </li>
                            <li>
                                <a href="#">PR Position <span class="fa arrow"></span></a>
                                <ul class="nav nav-third-level">
                                    <li>
                                        <a href="{{url('administrator/parameter/add-pr-position')}}"><i class="fa fa-plus"></i>Tambah</a>
                                    </li>
                                    <li>
                                        <a href="{{url('administrator/parameter/list-pr-position')}}"><i class="fa fa-table"></i>Senarai</a>
                                    </li>
                                </ul>
                            </li>
                            <li>
                                <a href="#">PR Organization <span class="fa arrow"></span></a>
                                <ul class="nav nav-third-level">
                                    <li>
                                        <a href="{{url('administrator/parameter/add-pr-organization')}}"><i class="fa fa-plus"></i>Tambah</a>
                                    </li>
                                    <li>
                                        <a href="{{url('administrator/parameter/list-pr-organization')}}"><i class="fa fa-table"></i>Senarai</a>
                                    </li>
                                </ul>
                            </li>
                            <li>
                                <a href="#">PR Dun <span class="fa arrow"></span></a>
                                <ul class="nav nav-third-level">
                                    <li>
                                        <a href="{{url('administrator/parameter/add-pr-dun')}}"><i class="fa fa-plus"></i>Tambah</a>
                                    </li>
                                    <li>
                                        <a href="{{url('administrator/parameter/list-pr-dun')}}"><i class="fa fa-table"></i>Senarai</a>
                                    </li>
                                </ul>
                            </li>
                            <li>
                                <a href="#">PR Parlimen <span class="fa arrow"></span></a>
                                <ul class="nav nav-third-level">
                                    <li>
                                        <a href="{{url('administrator/parameter/add-pr-parlimen')}}"><i class="fa fa-plus"></i>Tambah</a>
                                    </li>
                                    <li>
                                        <a href="{{url('administrator/parameter/list-pr-parlimen')}}"><i class="fa fa-table"></i>Senarai</a>
                                    </li>
                                </ul>
                            </li>
                            <li>
                                <a href="#">PR Senarai Bantuan <span class="fa arrow"></span></a>
                                <ul class="nav nav-third-level">
                                    <li>
                                        <a href="{{url('administrator/parameter/add-pr-beneficiary')}}"><i class="fa fa-plus"></i>Tambah</a>
                                    </li>
                                    <li>
                                        <a href="{{url('administrator/parameter/list-pr-beneficiary')}}"><i class="fa fa-table"></i>Senarai</a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                </li>

                <li>
                    <a href="#"><i class="fa fa-window-maximize"></i> <span class="nav-label">Main Web </span><span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level collapse">
                            <li>
                                <a href="#">Slider <span class="fa arrow"></span></a>
                                <ul class="nav nav-third-level">
                                    <li>
                                        <a href="{{url('administrator/web/slider/add')}}"><i class="fa fa-plus"></i>Tambah</a>
                                    </li>
                                    <li>
                                        <a href="{{url('administrator/web/slider/index')}}"><i class="fa fa-table"></i>Senarai</a>
                                    </li>
                                </ul>
                            </li>
                            <li>
                                <a href="#">Berita <span class="fa arrow"></span></a>
                                <ul class="nav nav-third-level">
                                    <li>
                                        <a href="{{url('administrator/web/news/add')}}"><i class="fa fa-plus"></i>Tambah</a>
                                    </li>
                                    <li>
                                        <a href="{{url('administrator/web/news/index')}}"><i class="fa fa-table"></i>Senarai</a>
                                    </li>
                                </ul>
                            </li>
                            <li>
                                <a href="#">Event <span class="fa arrow"></span></a>
                                <ul class="nav nav-third-level">
                                    <li>
                                        <a href="{{url('administrator/web/event/add')}}"><i class="fa fa-plus"></i>Tambah</a>
                                    </li>
                                    <li>
                                        <a href="{{url('administrator/web/event/index')}}"><i class="fa fa-table"></i>Senarai</a>
                                    </li>
                                </ul>
                            </li>
                            <li>
                                <a href="#">PR Parti Gabungan <span class="fa arrow"></span></a>
                                <ul class="nav nav-third-level">
                                    <li>
                                        <a href="{{url('administrator/parameter/add-pr-partigabungan')}}"><i class="fa fa-plus"></i>Tambah</a>
                                    </li>
                                    <li>
                                        <a href="{{url('administrator/parameter/list-pr-partigabungan')}}"><i class="fa fa-table"></i>Senarai</a>
                                    </li>
                                </ul>
                            </li>
                            <li>
                                <a href="#">PR Position <span class="fa arrow"></span></a>
                                <ul class="nav nav-third-level">
                                    <li>
                                        <a href="{{url('administrator/parameter/add-pr-position')}}"><i class="fa fa-plus"></i>Tambah</a>
                                    </li>
                                    <li>
                                        <a href="{{url('administrator/parameter/list-pr-position')}}"><i class="fa fa-table"></i>Senarai</a>
                                    </li>
                                </ul>
                            </li>
                            <li>
                                <a href="#">PR Organization <span class="fa arrow"></span></a>
                                <ul class="nav nav-third-level">
                                    <li>
                                        <a href="{{url('administrator/parameter/add-pr-organization')}}"><i class="fa fa-plus"></i>Tambah</a>
                                    </li>
                                    <li>
                                        <a href="{{url('administrator/parameter/list-pr-organization')}}"><i class="fa fa-table"></i>Senarai</a>
                                    </li>
                                </ul>
                            </li>
                            <li>
                                <a href="#">PR Dun <span class="fa arrow"></span></a>
                                <ul class="nav nav-third-level">
                                    <li>
                                        <a href="{{url('administrator/web/event/add')}}"><i class="fa fa-plus"></i>Tambah</a>
                                    </li>
                                    <li>
                                        <a href="{{url('administrator/web/event/index')}}"><i class="fa fa-table"></i>Senarai</a>
                                    </li>
                                </ul>
                            </li>
                            <li>
                                <a href="#">Gallery <span class="fa arrow"></span></a>
                                <ul class="nav nav-third-level">
                                    <li>
                                        <a href="{{url('administrator/web/gallery/add')}}"><i class="fa fa-plus"></i>Tambah</a>
                                    </li>
                                    <li>
                                        <a href="{{url('administrator/web/gallery/index')}}"><i class="fa fa-table"></i>Senarai</a>
                                    </li>
                                </ul>
                            </li>
                            
                        </ul>
                </li>
            </ul>
        </div>
    </nav>

        <div id="page-wrapper" class="gray-bg">
        <div class="row border-bottom">
        <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
        <div class="navbar-header">
            <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#"><i class="fa fa-bars"></i> </a>
            <form role="search" class="navbar-form-custom" action="search_results.html">
                <div class="form-group">
                    <input type="text" placeholder="Search for something..." class="form-control" name="top-search" id="top-search">
                </div>
            </form>
        </div>
            <ul class="nav navbar-top-links navbar-right">
                <li>
                    <span class="m-r-sm text-muted welcome-message">Welcome {{ Auth::user()->name }}</span>
                </li>
                
                <li>
                    <a href="{{route('logout')}}" onclick="event.preventDefault();
                                               document.getElementById('logout-form').submit();">
                        <i class="fa fa-sign-out"></i> Log out
                    </a>

                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>

                    
                </li>
            </ul>

        </nav>