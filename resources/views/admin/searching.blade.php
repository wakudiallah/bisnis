@extends('admin.layout.template')
@section('content_admin')


        </div>
            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-9">
                    <h2>	Search Page</h2>
                    <ol class="breadcrumb">
                        <li>
                            <a href="index.html">Home</a>
                        </li>
                        <li>
                            Extra Pages
                        </li>
                        <li class="active">
                            <strong>Search Page</strong>
                        </li>
                    </ol>
                </div>
            </div>
        <div class="wrapper wrapper-content animated fadeInRight">
            <div class="row">
                <div class="col-lg-12">
                    <div class="ibox float-e-margins">
                        <div class="ibox-content">
                            <!--<h2>
                                2,160 results found for: <span class="text-navy">“Admin Theme”</span>
                            </h2>-->
                           

                            <div class="search-form">
                                <form action="{{url('/searching_ic')}}" method="post">
                                    {{csrf_field()}}   
                                    <div class="input-group">
                                       <input type="text" placeholder="Search by IC" name="search" class="form-control input-lg"  required="" onkeyup="this.value = this.value.toUpperCase()" maxlength="12"  onKeyPress="return goodchars(event,'ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890',this)">
                                        <div class="input-group-btn">
                                            <button class="btn btn-lg btn-primary" type="submit">
                                                Search
                                            </button>
                                        </div>
                                    </div>
                                    <div class="col-lg-12" style="font-size: 24px !important">
                                        @if(Session::has('flash_message'))
                                            {!! session('flash_message') !!}
                                        @endif 
                                    </div> 
                                </form>
                            </div>
                           
                          
                        </div>
                    </div>
                </div>
        </div>
        </div>
        


  @endsection
