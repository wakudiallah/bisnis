@extends('admin.layout.template_datatables')

@section('content_admin')
              
    <div class="row wrapper border-bottom white-bg page-heading">
           
        <div class="col-lg-10">
            <h2>Parameter Parti</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="{{url('/administrator')}}">Home</a>
                </li>
                <li>
                    <a>Parameter</a>
                </li>

            </ol>
        </div>
        <div class="col-lg-2">
        </div>

    </div>


    <div class="wrapper wrapper-content animated fadeInRight">
        
        
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Parameter Parti</h5>
                        <div class="ibox-tools">
                          
                        </div>
                    </div>

                    <div class="ibox-content">
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover dataTables-example" >
                                <thead>
                                    <tr>
                                        <th width="10%">No</th>
                                        <th>Parti</th>
                                        <th>Status</th>
                                        <th width="20%">Tindakan</th>
                                    </tr>
                                </thead>
                              
                                <tbody>
                                <?php $i=1; ?>
                                @foreach($pr as $report)
                                    <tr class="gradeX">
                                        <td width="10%">{{$i}}</td>
                                        <td>{{$report->parti_gabungan_desc}}</td>
                                        <td>
                                            @if($report->status == 1)
                                                Aktif
                                            @else
                                                Non Aktif
                                            @endif
                                        </td>
                                        <td width="20%">
                                            <a href="{{url('/')}}/administrator/parameter/kemaskini-pr-parti/{{$report->id}}" type="submit" class="btn btn-warning" >
                                            <i class="fa fa-edit"></i> Kemaskini
                                            </a>
                                            <a href="{{url('/')}}/administrator/parameter/padam-pr-parti/{{$report->id}}" type="submit" class="btn btn-danger" >
                                            <i class="fa fa-trash"></i> Padam
                                            </a>
                                        </td>
                                    </tr>
                                <?php  $i++; ?>
                                @endforeach
                                </tbody>
                                
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection