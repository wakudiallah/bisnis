<div class="jarviswidget" id="wid-id-4" data-widget-editbutton="false" data-widget-custombutton="false">
    <!-- widget div-->
    <div>
        <div class="jarviswidget-editbox"></div>
            <div class="widget-body no-padding">
                <div id="checkout-form" class="smart-form" novalidate="novalidate">
                    <fieldset>
                        <div class="ibox-content">
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover dataTables-example" >
                                <thead>
                                    <tr>
                                       <td><b>Id  </b></td>
                                        <td><b>Nama  </b></td>
                                        <td><b>Jawatan</b></td>
                                        <td><b>MyKad</b></td>
                                        <td><b>Nama DM</b></td>  
                                        <td><b>Nama Dun</b></td>  
                                        <td><b>Tel Bimbit</b></td>
                                    </tr>
                                </thead>
                              
                                <tbody>
                                <?php $i=1; ?>
                                @foreach($ajk as $ajk)
                                    <tr class="gradeX">
                                        <td>{{$i}}</td>
                                        <td>{{$ajk->name}}</td>
                                        <td>{{$ajk->jawatan->desc}}</td>
                                        <td>{{$ajk->ic}}</td>
                                        <td>{{$ajk->NamaDM}}</td>
                                        <td>{{$ajk->NamaDun}}</td>
                                        <td>{{$ajk->phone}}</td>
                                        </tr>
                                <?php  $i++; ?>
                                @endforeach
                                </tbody>
                                <!--<tfoot>
                                    <tr>
                                        <th>No</th>
                                        <th>Nama</th>
                                        <th>IC</th>
                                        <th>Nama DUN</th>
                                        <th>Petugas</th>
                                        <th>Tindakan</th>
                                    </tr>
                                </tfoot>-->
                            </table>
                        </div>
                    </div>
                        <footer>
                            <div class="ibox-content">
                                <div class="text-center">
                                    <a data-toggle="modal" class="btn btn-primary" href="#modal-form">Tambah Jawatan</a>
                                </div>
                                <div id="modal-form" class="modal fade" aria-hidden="true">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-body">
                                                <div class="row">
                                                    <div class="col-sm-12 b-r"><h3 class="m-t-none m-b">Tambah Jawatan</h3>
                                                       
                                                         <form class="form-horizontal" method="post" action="{{url('/administrator/organization/save_detail')}}">
                                                        {{csrf_field()}}
                                                           <input type="hidden" class="form-control" name="org_id" required="" id="id" value="{{$org->id}}">
                                                            <div class="form-group">
                                                                <label>MyKad</label> 
                                                                    <input type="text" class="form-control" name="ic" required="" id="ic">
                                                            </div>
                                                            <div class="form-group">
                                                                <label>Nama</label> 
                                                                    <input type="text" class="form-control" name="name" required="" id="name">
                                                            </div>
                                                            <div class="form-group">
                                                                <label>Jawatan</label> 
                                                                    <select class="form-control" name="position">
                                                                        @foreach($position as $position)
                                                                            <option value="{{$position->id}}">{{$position->desc}}</option>
                                                                        @endforeach
                                                                    </select>
                                                            </div>
                                                            <div class="form-group">
                                                                <label>Tel Bimbit</label> 
                                                                    <input type="text" class="form-control" name="phone" required="" id="phone">
                                                            </div>
                                                            <div class="form-group">
                                                                <label>Nama DM</label> 
                                                                    <input type="text" class="form-control" name="NamaDM_ajk" required="" id="NamaDM_ajk">
                                                            </div>
                                                            <div class="form-group">
                                                                <label>Nama DUN</label> 
                                                                    <input type="text" class="form-control" name="NamaDUN_ajk" required="" id="NamaDUN_ajk">
                                                            </div>
                                                             <div class="form-group">
                                                                <label>Nama Parlimen</label> 
                                                                    <input type="text" class="form-control" name="NamaParlimen_ajk" required="" id="NamaParlimen_ajk">
                                                            </div>
                                                            <div>
                                                                  <button class="btn btn-primary" type="submit">Simpan</button>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </footer>
                    </fieldset>
                </div>
            </div>
        </div>
    </div>