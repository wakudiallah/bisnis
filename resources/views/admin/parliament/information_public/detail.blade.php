@extends('admin.layout.template_multiselect')
@section('content_admin')
    </div>
        <div class="row wrapper border-bottom white-bg page-heading">
            <div class="col-lg-10">
                <h2>Lihat Maklumat Rakyat</h2>
                <ol class="breadcrumb">
                    <li>
                        <a href="index.html">Dashboard</a>
                    </li>
                    <li>
                        <a>Maklumat Rakyat</a>
                    </li>
                    <li class="active">
                        <strong>Lihat</strong>
                    </li>
                </ol>
            </div>
            <div class="col-lg-2">
            </div>
        </div>
        <div class="wrapper wrapper-content animated fadeInRight">
            
            <div class="row">
                <div class="col-lg-12">
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <h5 style="text-align: center !important;">MAKLUMAT RAKYAT MALAYSIA</h5>
                        </div>
                        <div class="ibox-content">
                            <form class="form-horizontal" method="post" action="{{url('/administrator/information_public/save')}}">
                            {{csrf_field()}}
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Nama Penuh</label>
                                    <div class="col-sm-10">
                                        <input id="name" name="name" type="text" class="form-control" value="{{$cif->name}}" onkeyup="this.value = this.value.toUpperCase()" readonly="">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">IC Baru</label>
                                    <div class="col-sm-4"> 
                                        <input id="ic" name="ic" type="text" class="form-control required" value="{{$cif->ic}}" readonly="">
                                    </div>
                                     <label class="col-sm-2 control-label">IC Lama</label>
                                    <div class="col-sm-4"> 
                                        <input id="iclama" name="ICLama" type="text" class="form-control required" value="{{$cif->ICLama}}" readonly="">
                                    </div>
                                </div>
                                <div class="form-group">
                                   
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Tarikh Lahir</label>
                                    <div class="col-sm-4">
                                        <input id="dob" name="dob" type="text" class="form-control required" value="{{$cif->dob}}" readonly="">
                                    </div>
                                     <label class="col-sm-2 control-label">Jantina</label>
                                    <div class="col-sm-4">
                                        <select name="gender"  class="form-control" readonly="">
                                             @if(!empty($cif->gender))
                                                @if($cif->gender=='L')
                                                <option value="L">L</option>
                                                @elseif($cif->gender=='P')
                                                <option value="P">P</i></option>
                                                @endif
                                            @endif
                                                <option value="L">L</option>
                                                <option value="P">P</i></option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Status Perkahwinan</label>
                                    <div class="col-sm-4">
                                       <select name="marital_status"  class="form-control m-b" id="marital_status" readonly="">
                                        @foreach($marital as $marital)
                                          @if($cif->marital_status !=NULL)
                                              <?php 
                                                if($cif->marital_status==$marital->marital_code) {
                                                  $selected = "selected";
                                                }
                                                else {
                                                   $selected = "";
                                                } 
                                              ?>
                                              <option {{$selected}} value="{{$marital->marital_code}}">{{$marital->marital_desc}}</option>
                                          @else 
                                                <option value="{{$marital->marital_code}}">{{$marital->marital_desc}}</option>
                                          @endif
                                        @endforeach
                                        </select>
                                    </div>
                                     <label class="col-sm-2 control-label">Status Kediaman</label>
                                    <div class="col-sm-4">
                                       <select name="residence"  class="form-control m-b" id="residence" readonly="">
                                        @foreach($residence as $residence)
                                          @if($cif->residence !=NULL)
                                              <?php 
                                                if($cif->residence==$residence->code) {
                                                  $selected = "selected";
                                                }
                                                else {
                                                   $selected = "";
                                                } 
                                              ?>
                                              <option {{$selected}} value="{{$residence->code}}">{{$residence->desc}}</option>
                                          @else 
                                                <option value="{{$residence->code}}">{{$residence->desc}}</option>
                                          @endif
                                        @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="hr-line-dashed"></div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Nama Lokaliti</label>
                                    <div class="col-sm-10">
                                        <input id="lokaliti" name="lokaliti" type="text" class="form-control required" value="{{$cif->NamaLokaliti}}" readonly="">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Nama Dun</label>
                                    <div class="col-sm-4">
                                        <input id="dun" name="dun" type="text" class="form-control required" value="{{$cif->NamaDun}}" readonly="">
                                    </div>
                                    <label class="col-sm-2 control-label">Nama DM</label>
                                    <div class="col-sm-4">
                                        <input id="dms" name="dms" type="text" class="form-control required" value="{{$cif->NamaDM}}" readonly="">
                                    </div>
                                </div>
                                <div class="form-group">
                                     <label class="col-sm-2 control-label">Nama Parlimen</label>
                                    <div class="col-sm-4">
                                        <input id="NamaParlimen" name="NamaParlimens" type="text" class="form-control required" value="{{$cif->NamaParlimen}}" readonly="">
                                    </div>
                                    <label class="col-sm-2 control-label">Negeri</label>
                                    <div class="col-sm-4">
                                        <input id="negeri" name="negeri" type="text" class="form-control required" value="{{$cif->Negeri}}" readonly="">
                                    </div>
                                </div>
                                <input id="norumah" name="norumah" type="hidden" class="form-control required" value="{{$cif->NoRumah}}" readonly="">
                                <div class="hr-line-dashed"></div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">No Telp Bimbit 1</label>
                                    <div class="col-sm-4">  
                                        <input id="mobile" name="mobile" type="text" class="form-control" maxlength="12" value="{{$cif->mobile}}" onkeypress="return isNumberKey(event)" readonly="">
                                    </div>
                                     <label class="col-sm-2 control-label">No Telp Bimbit 2</label>
                                    <div class="col-sm-4">  
                                        <input id="mobile2" name="mobile2" type="text" class="form-control" maxlength="12" value="{{$cif->mobile2}}" onkeypress="return isNumberKey(event)" readonly="">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">No Telp Rumah</label>
                                    <div class="col-sm-4">  
                                        <input id="telephone" name="telephone" type="text" class="form-control" maxlength="12" value="{{$cif->telephone}}" onkeypress="return isNumberKey(event)" readonly="">
                                    </div>
                                    <label class="col-sm-2 control-label">Email</label>
                                    <div class="col-sm-4">  
                                        <input id="email" name="email" type="email" class="form-control" value="{{$cif->email}}" readonly="">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Pekerjaan</label>
                                    <div class="col-sm-4">
                                        <select name="occupation_id" id="occupation_id" data-placeholder="x" class="form-control" style="width:350px;" tabindex="4" readonly="">
                                        @foreach($occupation as $occupation)
                                          @if($cif->occupation_id !=NULL)
                                              <?php 
                                                if($cif->occupation_id==$occupation->id) {
                                                  $selected = "selected";
                                                }
                                                else {
                                                   $selected = "";
                                                } 
                                              ?>
                                              <option {{$selected}} value="{{$occupation->id}}">{{$occupation->desc}}</option>
                                          @else 
                                                <option value="{{$occupation->id}}">{{$occupation->desc}}</option>
                                          @endif
                                        @endforeach
                                        </select>
                                    </div>
                                    <label class="col-sm-2 control-label">Penyakit</label>
                                    <div class="col-sm-4">
                                            @foreach($sick as $sick)
                                               {{$sick->disease->disease_desc}}
                                            @endforeach
                                        <input type="hidden" name="x[]" id="a" value="">
                                    </div>
                                </div>
                               
                                 
                                <div class="hr-line-dashed"></div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Jumlah Tanggungan</label>
                                    <div class="col-sm-4">
                                        <input type="text" name="no_dependants" id="no_dependants"min="0" inputmode="numeric" pattern="[0-9]*"  max="20"  @if (Session::has('no_dependants'))   @endif readonly="" class="form-control fn" onkeypress="return isNumberKey(event)"  value="{{$cif->no_dependants}}">
                                    </div>
                                    <label class="col-sm-2 control-label">Status Tanggungan</label>
                                    <div class="col-sm-4">
                                        <input type="text" id="school_dep" name="school_dep" class="form-control fn" placeholder="Sekolah"  min="0" inputmode="numeric" pattern="[0-9]*"  max="20" @if (Session::has('school_dep'))   @endif readonly="" onkeypress="return isNumberKey(event)" value="{{$cif->school_dep}}"><br>

                                        <input type="text" id="working_dep" name="working_dep" class="form-control fn" placeholder="Bekerja"  min="0" inputmode="numeric" pattern="[0-9]*" max="20" @if (Session::has('working_dep'))   @endif readonly="" onkeypress="return isNumberKey(event)" value="{{$cif->working_dep}}"><br>

                                        <input type="text" id="unemployee_dep" name="unemployee_dep" class="form-control fn" placeholder="Tidak Bekerja" min="0" inputmode="numeric" pattern="[0-9]*"  max="20" @if (Session::has('unemployee_dep'))   @endif readonly="" onkeypress="return isNumberKey(event)" value="{{$cif->unemployee_dep}}"><br>
                                    </div>
                                    <div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Penerima Bantuan</label>
                                    <div class="col-sm-4">
                                       <select name="beneficiary"  class="form-control m-b" id="one" readonly="">
                                            @if(!empty($cif->beneficiary))
                                                @if($cif->beneficiary=='Ya')
                                                <option  value="Ya">Ya</option>
                                                @elseif($cif->beneficiary=='Tidak')
                                                <option  value="Tidak">Tidak</option> 
                                                @endif
                                            @endif
                                                <option  value="Ya">Ya</option>
                                                <option  value="Tidak">Tidak</option> 
                                        </select>
                                    </div>
                                    @if($cif->beneficiary=='Ya')
                                        <label class="col-sm-2 control-label resources" >Remark</label>
                                        <div class="col-sm-4 resources" >
                                            <textarea id='remark_beneficiary' class="form-control" rows="3"  style="color: black" name='remark_beneficiary'  onkeyup="this.value = this.value.toUpperCase()" readonly="">{{$cif->remark_beneficiary}}</textarea>
                                        </div>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Alamat 1</label>
                                    <div class="col-sm-10">
                                        <input type="text" name="address_1" id="address_1" required="" class="form-control" value="{{$cif->Address->address_1}}" readonly="">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Alamat 2</label>
                                    <div class="col-sm-10">
                                         <input type="text" name="address_2" id="address_2" required="" class="form-control" value="{{$cif->Address->address_2}}" readonly="">
                                    </div>
                                </div>
                                 <div class="form-group">
                                    <label class="col-sm-2 control-label">Alamat 3</label>
                                    <div class="col-sm-10">
                                         <input type="text" name="address_3" id="address_3" required="" class="form-control" value="{{$cif->Address->address_3}}" readonly="">
                                    </div>
                                </div>
                                <div class="form-group">
                                        <label class="col-sm-2 control-label"></label>
                                            <div class="col-md-3">
                                                <p class="font-bold">
                                                    Poskod
                                                </p>
                                                <input type="text" name="postcode" id="postcode" min="0" inputmode="numeric" pattern="[0-9]*"  maxlength="6"  class="form-control" readonly="" value="{{$cif->Address->postcode}}">
                                            </div>
                                            <div class="col-md-3">
                                                <p class="font-bold">
                                                    Bandar
                                                </p>
                                                <input type="text" name="city" id="city" readonly="" class="form-control" value="{{$cif->Address->city}}">
                                            </div>
                                            <div class="col-md-4">
                                                <p class="font-bold">
                                                    Negeri
                                                </p>
                                                <input type="text" name="state" id="state" readonly="" class="form-control" value="{{$cif->Address->state}}">
                                            </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Kecenderungan</label>
                                    <div class="col-sm-10">
                                       @if($cif->kecenderungan == '1')
                                         <div class="radio radio-info radio-inline">
                                            <input type="radio" id="inlineRadiop" value="1" name="kecenderungan"  checked disabled>
                                            <label for="inlineRadiop"> Putih </label>
                                        </div>
                                        <div class="radio radio-inline">
                                            <input type="radio" id="inlineRadiok" value="2" name="kecenderungan" disabled="">
                                            <label for="inlineRadiok"> Kelabu </label>
                                        </div>
                                        <div class="radio radio-inline">
                                            <input type="radio" id="inlineRadioh" value="3" name="kecenderungan" disabled="">
                                            <label for="inlineRadioh"> Hitam </label>
                                        </div>

                                        @elseif($cif->kecenderungan == '3'  )
                                         <div class="radio radio-info radio-inline">
                                            <input type="radio" id="inlineRadiop" value="1" name="kecenderungan"  disabled="">
                                            <label for="inlineRadiop"> Putih </label>
                                        </div>
                                        <div class="radio radio-inline">
                                            <input type="radio" id="inlineRadiok" value="2" name="kecenderungan" disabled="">
                                            <label for="inlineRadiok"> Kelabu </label>
                                        </div>
                                        <div class="radio radio-inline">
                                            <input type="radio" id="inlineRadioh" value="3" name="kecenderungan" checked disabled>
                                            <label for="inlineRadioh"> Hitam </label>
                                        </div>

                                        @elseif($cif->kecenderungan == '2'  )
                                         <div class="radio radio-info radio-inline">
                                            <input type="radio" id="inlineRadiop" value="1" name="kecenderungan" disabled=""  >
                                            <label for="inlineRadiop"> Putih </label>
                                        </div>
                                        <div class="radio radio-inline">
                                            <input type="radio" id="inlineRadiok" value="2" name="kecenderungan" checked disabled>
                                            <label for="inlineRadiok" > Kelabu </label>
                                        </div>
                                        <div class="radio radio-inline">
                                            <input type="radio" id="inlineRadioh" value="3" name="kecenderungan" disabled="" >
                                            <label for="inlineRadioh"> Hitam </label>
                                        </div>
                                        @endif

                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Lain Lain Issue</label>
                                    <div class="col-sm-10">
                                        <textarea id='issue' class="form-control" rows="3" style="margin-top: 0px; margin-bottom: 0px; height: 183px;" style="color: black" name='issue' readonly="">{{$cif->issue}}</textarea>   
                                    </div>
                                </div>
                                <div class="hr-line-dashed"></div>
                                <div class="form-group">
                                    <div class="col-sm-4 col-sm-offset-2">
                                        <!--<button class="btn btn-white" type="submit">Cancel</button>-->
                                        <a href="{{url('/administrator/information_public/list')}}" class="btn btn-primary" type="submit">Kembali</a>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
@endsection

@push('custom')
<script>
function isNumberKey(evt){
    var charCode = (evt.which) ? evt.which : event.keyCode
    if (charCode > 31 && (charCode < 48 || charCode > 57))
        return false;
    return true;
}
</script>
<script type="text/javascript">
  /*  $( ".fn" ).keyup(function() {
var no_dependants = $('#no_dependants').val();

var school_dep = $('#school_dep').val();

var unemployee_dep = $('#unemployee_dep').val();

var working_dep = $('#working_dep').val();

var total =  parseFloat(school_dep) + parseFloat(unemployee_dep) +parseFloat(working_dep);

   $("#total").val(parseFloat(total));

   if(total != no_dependants){
     alert("You must agree to the terms and conditions");
   }
});*/
</script>
@endpush
