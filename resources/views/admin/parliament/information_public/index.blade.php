@extends('admin.layout.template')

@section('content_admin')
</div>
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-9">
            <h2>Laman Pencarian</h2>
                <ol class="breadcrumb">
                    <li>
                        <a href="index.html">Home</a>
                    </li>
                    <li class="active">
                        <strong>Laman Pencarian</strong>
                    </li>
                </ol>
        </div>
    </div>
    @include('admin.shared.notif') 

    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-content">
                        <div class="search-form">
                            <form action="{{url('/administrator/information_public/searching_ic')}}" method="post">
                            {{csrf_field()}}
                                <div class="input-group">
                                    <input type="text" placeholder="Search by IC, Old IC or Name " name="search" class="form-control input-lg"  required="" onkeyup="this.value = this.value.toUpperCase()">
                                    <div class="input-group-btn">
                                        <button class="btn btn-lg btn-primary" type="submit">
                                            Cari
                                        </button>
                                    </div>
                                </div>
                                <div class="col-lg-12" style="font-size: 24px !important">
                                    @if(Session::has('flash_message'))
                                        {!! session('flash_message') !!}
                                    @endif 
                                </div> 
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
  @endsection