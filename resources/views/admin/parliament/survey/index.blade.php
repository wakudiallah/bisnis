@extends('admin.layout.template_datatables')

@section('content_admin')
              <style>
.button {
  background-color: #4CAF50; /* Green */
  border: none;
  color: white;
  padding: 15px 32px;
  text-align: center;
  text-decoration: none;
  display: inline-block;
  font-size: 16px;
  margin: 4px 2px;
  cursor: pointer;
}

.button2 {background-color: #008CBA;} /* Blue */
.button3 {background-color: #f44336;} /* Red */ 
.button4 {background-color: #e7e7e7; color: black;} /* Gray */ 
.button5 {background-color: #555555;} /* Black */
</style>

    <div class="wrapper wrapper-content animated fadeInRight">
        @include('admin.shared.notif')
        
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>PARLIMEN - SENARAI KECENDERUNGAN RAKYAT </h5>
                        <div class="ibox-tools">
                          
                        </div>
                    </div>

                    <div class="ibox-content">
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover dataTables-example" >
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Nama</th>
                                        <th>IC</th>
                                        <th>Kecenderungan</th>
                                        <th>Nama DM</th>
                                        <th>Negeri</th>
                                        <th>Tarikh</th>
                                        <th>Petugas</th>
                                        <th>Tindakan</th>
                                    </tr>
                                </thead>
                              
                                <tbody>
                                <?php $i=1; ?>
                                @foreach($survey as $survey)
                                    <tr class="gradeX">
                                        <td>{{$i}}</td>
                                        <td>{{$survey->name}}</td>
                                        <td>{{$survey->ic}}</td>
                                        @if($survey->kecenderungan=='1')
                                        <td>   <button class="button button3">Putih</button></td>
                                        @elseif($survey->kecenderungan=='3')
                                        <td> <button class="button button5">Hitam</button></td>
                                        @elseif($survey->kecenderungan=='2')
                                        <td> <button class="button button4">Kelabu</button></td>
                                        @endif
                                        <td>{{$survey->NamaDM}}</td>
                                        <td>{{$survey->Negeri}}</td>
                                        <td>{{$survey->created_at}}</td>
                                        <td>{{$survey->user_petugas->name}}</td>
                                        <td class="center">
                                            <a href="{{url('/')}}/administrator/survey/detail/{{$survey->ic}}" type="submit" class="btn btn-danger" style="cursor:pointer;">
                                            <i class="fa fa-paste"></i> Detail
                                            </a>
                                        </td>
                                    </tr>
                                <?php  $i++; ?>
                                @endforeach
                                </tbody>
                                <!--<tfoot>
                                    <tr>
                                        <th>No</th>
                                        <th>Nama</th>
                                        <th>IC</th>
                                        <th>Nama DUN</th>
                                        <th>Petugas</th>
                                        <th>Tindakan</th>
                                    </tr>
                                </tfoot>-->
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection