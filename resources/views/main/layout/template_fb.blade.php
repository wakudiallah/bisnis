<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />

	<title>Facebook Style Home Page Design - demo by w3lessons.info</title>
	<link rel="stylesheet" href="{{asset('templatefb/style.css')}}" type="text/css" />
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

</head>
<body class="login">
<!-- header starts here -->
	<div id="facebook-Bar">
		<div id="facebook-Frame">
			<div id="logo">
				<a href="#">SMARAK</a>
			</div>
			<div id="header-main-right">

		<div id="header-main-right-nav">
			<form method="post" action="" id="login_form" name="login_form">
				<table border="0" style="border:none">
					<tr>
						<td>
							<input type="text" tabindex="1" id="email" placeholder="Email or Phone" name="email" class="inputtext radius1" value="">
						</td>
						<td>
							<input type="password" tabindex="2" id="pass" placeholder="Password" name="pass" class="inputtext radius1" >
						</td>
						<td>
							<input type="submit" class="fbbutton" name="login" value="Login" />
						</td>
					</tr>
					<tr>
						<td>
							<label>
							<input id="persist_box" type="checkbox" name="persistent" value="1" checked="1"/>
							<span style="color:#ccc;">Keep me logged in</span></label>
						</td>
						<td>
							<label><a href="" style="color:#ccc; text-decoration:none">forgot your password?</a></label>
						</td>
					</tr>
				</table>
			</form>
		</div>
	</div>
</div>
</div>
<!-- header ends here -->
<div class="loginbox radius">
<h2 style="color:#141823; text-align:center;">Welcome to Facebook</h2>
<div class="loginboxinner radius">
<div class="loginheader">
<h4 class="title">Connect with friends and the world around you.</h4>
</div>
<!--loginheader-->
<div class="loginform">
<form id="login" action="" method="post">
<p>
<input type="text" id="firstname" name="firstname" placeholder="First Name" value="" class="radius mini" />
<input type="text" id="lastname" name="lastname" placeholder="Last Name" value="" class="radius mini" />
</p>
<p>
<input type="text" id="email" name="email" placeholder="Your Email" value="" class="radius" />
</p>
<p>
<input type="text" id="remail" name="remail" placeholder="Re-enter Email" class="radius" />
</p>
<p>
<input type="password" id="password" name="password" placeholder="New Password" class="radius" />
</p>
<p>
<button class="radius title" name="signup">Sign Up for Facebook</button>
</p>
</form>
</div>
<!--loginform-->
</div>
<!--loginboxinner-->
</div>
	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
<!--loginbox-->
</body>
</html>