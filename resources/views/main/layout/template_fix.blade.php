<!DOCTYPE html>
<html lang="en">
<head>
<title>SMARAK</title>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="description" content="Colo Shop Template">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" type="text/css" href="{{asset('main/styles/bootstrap4/bootstrap.min.css')}}">
<link href="{{asset('main/plugins/font-awesome-4.7.0/css/font-awesome.min.css')}}" rel="stylesheet" type="text/css">
<link rel="stylesheet" type="text/css" href="{{asset('main/plugins/OwlCarousel2-2.2.1/owl.carousel.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('main/plugins/OwlCarousel2-2.2.1/owl.theme.default.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('main/plugins/OwlCarousel2-2.2.1/animate.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('main/styles/main_styles.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('main/styles/responsive.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('main/styles/categories_styles.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('main/styles/categories_responsive.css')}}">


<link rel="stylesheet" href="{{asset('main/plugins/themify-icons/themify-icons.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('main/plugins/jquery-ui-1.12.1.custom/jquery-ui.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('main/styles/contact_styles.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('main/styles/contact_responsive.css')}}">
<link rel="icon" href="{{asset('main/images/favicon.ico')}}" type="image/x-icon">


</head>

<body>

<div class="super_container">

	<!-- Header -->

	@include('main/layout/header');

	<div class="fs_menu_overlay"></div>
	<div class="hamburger_menu">
		<div class="hamburger_close"><i class="fa fa-times" aria-hidden="true"></i></div>
		<div class="hamburger_menu_content text-right">
			<ul class="menu_top_nav">
				<li class="menu_item has-children">
					<a href="#">
						usd
						<i class="fa fa-angle-down"></i>
					</a>
					<ul class="menu_selection">
						<li><a href="#">cad</a></li>
						<li><a href="#">aud</a></li>
						<li><a href="#">eur</a></li>
						<li><a href="#">gbp</a></li>
					</ul>
				</li>
				<li class="menu_item has-children">
					<a href="#">
						English
						<i class="fa fa-angle-down"></i>
					</a>
					<ul class="menu_selection">
						<li><a href="#">French</a></li>
						<li><a href="#">Italian</a></li>
						<li><a href="#">German</a></li>
						<li><a href="#">Spanish</a></li>
					</ul>
				</li>
				<li class="menu_item has-children">
					<a href="#">
						My Account
						<i class="fa fa-angle-down"></i>
					</a>
					<ul class="menu_selection">
						<li><a href="#"><i class="fa fa-sign-in" aria-hidden="true"></i>Sign In</a></li>
						<li><a href="#"><i class="fa fa-user-plus" aria-hidden="true"></i>Register</a></li>
					</ul>
				</li>
				

			</ul>
		</div>
	</div>

	<!-- alert -->
	@include('sweetalert::alert')


	@yield('content')


	<div class="deal_ofthe_week">
		<div class="container">
			<div class="row align-items-center">
				<!-- <div class="col-lg-6">
					<div class="deal_ofthe_week_img">
						<img src="{{asset('main/images/deal_ofthe_week.png')}}" alt=""> 
					</div>
				</div>-->
				<!--<div class="col-lg-12 text-right deal_ofthe_week_col" style="margin-top: 60px !important; margin-bottom: 60px !important">
					<div class="deal_ofthe_week_content d-flex flex-column align-items-center float-center">
						<div class="section_title">
							<h2>Malaysian 14th General Election Count Up</h2>
						</div>
						<ul class="timer">
							<li class="d-inline-flex flex-column justify-content-center align-items-center">
								<div id="day" class="timer_num">03</div>
								<div class="timer_unit">{{trans('home/index.day')}}</div>
							</li>
							<li class="d-inline-flex flex-column justify-content-center align-items-center">
								<div id="hour" class="timer_num">15</div>
								<div class="timer_unit">{{trans('home/index.hour')}}</div>
							</li>
							<li class="d-inline-flex flex-column justify-content-center align-items-center">
								<div id="minute" class="timer_num">45</div>
								<div class="timer_unit">{{trans('home/index.min')}}</div>
							</li>
							<li class="d-inline-flex flex-column justify-content-center align-items-center">
								<div id="second" class="timer_num">23</div>
								<div class="timer_unit">{{trans('home/index.sec')}}</div>
							</li>
						</ul>
						
					</div>
				</div>-->
			</div>

			<div class="row align-items-center">
				<!-- <div class="col-lg-6">
					<div class="deal_ofthe_week_img">
						<img src="{{asset('main/images/deal_ofthe_week.png')}}" alt=""> 
					</div>
				</div>-->
				<div class="col-lg-12 text-right deal_ofthe_week_col" style="margin-bottom: 60px !important">
					<div class="deal_ofthe_week_content d-flex flex-column align-items-center float-center">
						<div class="section_title">
							

							<h2>PRK {{trans('home/index.start')}}</h2>
						</div>
						<ul class="timer">
							<li class="d-inline-flex flex-column justify-content-center align-items-center">
								<div id="day" class="timer_num">12</div>
								<div class="timer_unit">{{trans('home/index.day')}}</div>
							</li>
							<li class="d-inline-flex flex-column justify-content-center align-items-center">
								<div id="hour" class="timer_num">0</div>
								<div class="timer_unit">{{trans('home/index.hour')}}</div>
							</li>
							<li class="d-inline-flex flex-column justify-content-center align-items-center">
								<div id="minute" class="timer_num">0</div>
								<div class="timer_unit">{{trans('home/index.min')}}</div>
							</li>
							<li class="d-inline-flex flex-column justify-content-center align-items-center">
								<div id="second" class="timer_num">0</div>
								<div class="timer_unit">{{trans('home/index.sec')}}</div>
							</li>
						</ul>
						
					</div>
				</div>
			</div>


	
		</div>
	</div>
	

	<div class="newsletter">
		<div class="container">
			<div class="row">
				<div class="col-lg-6">
					<div class="newsletter_text d-flex flex-column justify-content-center align-items-lg-start align-items-md-center text-center">
						<h4>Newsletter</h4>
						<p>Subscribe to our get our newsletter</p>
					</div>
				</div>
				<div class="col-lg-6">
					<form action="post">
						<div class="newsletter_form d-flex flex-md-row flex-column flex-xs-column align-items-center justify-content-lg-end justify-content-center">
							<input id="newsletter_email" type="email" placeholder="Your email" required="required" data-error="Valid email is required.">
							<button id="newsletter_submit" type="submit" class="newsletter_submit_btn trans_300" value="Submit">subscribe</button>
						</div>
					</form>
				</div>
			</div>

		</div>
	</div>

	<!-- Footer -->

	<footer class="footer">
		<div class="container">
			<div class="row">
				<div class="col-lg-6">
					<div class="footer_nav_container d-flex flex-sm-row flex-column align-items-center justify-content-lg-start justify-content-center text-center">
						<ul class="footer_nav">
							<li><a href="#">{{trans('menu/menu.faq')}}</a></li>
							<li><a href="contact.html">{{trans('menu/menu.contact')}}</a></li>
						</ul>
					</div>
				</div>
				<div class="col-lg-6">
					<div class="footer_social d-flex flex-row align-items-center justify-content-lg-end justify-content-center">
						<ul>
							<li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
							<li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
							<li><a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
							<li><a href="#"><i class="fa fa-skype" aria-hidden="true"></i></a></li>
							<li><a href="#"><i class="fa fa-pinterest" aria-hidden="true"></i></a></li>
						</ul>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-12">
					<div class="footer_nav_container">
						<div class="cr"> © {{ date('Y') }} All Rights Reserverd. By <a href="https://netxpert.my">NetXpert Sdn Bhd</a> </div>
					</div>
				</div>
			</div>
		</div>
	</footer>

</div>

<script src="{{asset('main/js/jquery-3.2.1.min.js')}}"></script>
<script src="{{asset('main/styles/bootstrap4/popper.js')}}"></script>
<script src="{{asset('main/styles/bootstrap4/bootstrap.min.js')}}"></script>
<script src="{{asset('main/plugins/Isotope/isotope.pkgd.min.js')}}"></script>
<script src="{{asset('main/plugins/OwlCarousel2-2.2.1/owl.carousel.js')}}"></script>
<script src="{{asset('main/plugins/easing/easing.js')}}"></script>
<script src="{{asset('main/js/custom.js')}}"></script>
   <script type="text/javascript">
        $( "#postcode" ).change(function() {
            var postcode = $('#postcode').val();
            $.ajax({
            url: "<?php  print url('/'); ?>/administrator/postcode/"+postcode,
            dataType: 'json',
            data: {
            },
            success: function (data, status) {
                jQuery.each(data, function (k) {
                    $("#city").val(data[k].post_office );
                    $("#state").val(data[k].state.state_name );
                    $("#country").val("Malaysia");
                    });
                }
            });
        });
   	</script>
    <script type="text/javascript">
            $( "#ic" ).change(function() {
                var ic = $('#ic').val();
                $.ajax({
                url: "<?php  print url('/'); ?>/administrator/ic/"+ic,
                dataType: 'json',
                data: {
                },
                success: function (data, status) {
                    jQuery.each(data, function (k) {
                        $("#nama").val(data[k].Nama );
                        });
                    }
                });
            });
    </script>


    <script type="text/javascript">
        $( "#ic_reg" ).change(function() {
            var ic_reg = $('#ic_reg').val();
            $.ajax({
            url: "<?php  print url('/'); ?>/administrator/ic_reg/"+ic_reg,
            dataType: 'json',
            data: {
            },
            success: function (data, status) {
                jQuery.each(data, function (k) {
                    $("#name_reg").val(data[k].Nama );
                    $("#kodlokaliti_reg").val(data[k].Kodlokaliti );
                    $("#Negeri_reg").val(data[k].Negeri );
                    $("#NamaLokaliti_reg").val(data[k].NamaLokaliti );
                    $("#NamaDM_reg").val(data[k].NamaDM );
                    $("#NamaDUN_reg").val(data[k].NamaDun );
                    $("#NamaParlimen_reg").val(data[k].NamaParlimen );

                    });
                }
            });
        });
    </script>
     <input type="hidden" name="_token" id="_token" value="{{csrf_token()}}">

        <script>
        var apiGeolocationSuccess = function(position) {
          showLocation(position);
        };
        var tryAPIGeolocation = function() {
          jQuery.post( "https://www.googleapis.com/geolocation/v1/geolocate?key=AIzaSyATzsafy5WHjV4871j-qZmJYfZXXlrxSv0", function(success) {
            apiGeolocationSuccess({coords: {latitude: success.location.lat, longitude: success.location.lng}});
          })
          .fail(function(err) {
            // alert("API Geolocation error! \n\n"+err);
            window.location.href = "/geolocation/error/"+err;
          });
        };

        var browserGeolocationSuccess = function(position) {
          showLocation(position);
        };

        var browserGeolocationFail = function(error) {
          switch (error.code) {
            case error.TIMEOUT:
              alert("Browser geolocation error !\n\nTimeout.");
              break;
            case error.PERMISSION_DENIED:
              if(error.message.indexOf("Only secure origins are allowed") == 0) {
                tryAPIGeolocation();
              }
                 window.location.href = "{{url('/')}}/geolocation/error/"+error.code;
              break;
            case error.POSITION_UNAVAILABLE:
              alert("Browser geolocation error !\n\nPosition unavailable.");
              break;
          }
        };

        var tryGeolocation = function() {
          if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(
              browserGeolocationSuccess,
              browserGeolocationFail,
              {maximumAge: 50000, timeout: 20000, enableHighAccuracy: true});
          }
        };

        tryGeolocation();

        function showLocation(position) {
          var latitude = position.coords.latitude;
          var longitude = position.coords.longitude;
          var accuracy = position.coords.accuracy;
          
          var _token = $('#_token').val();
          $.ajax({
            type:'POST',
            url: "{{url('/')}}/getLocation",
            data: { latitude: latitude, _token : _token, longitude : longitude },
            success:function(data){
                    if(data){
                      $("#latitude").html("<input type='hidden' name='latitude' id='latitude' value='"+data.latitude+"'>");
                      $("#longitude").html("<input type='hidden' name='longitude' id='longitude' value='"+data.longitude+"'>");
                       $("#location").html("<input type='hidden' name='location' id='location' value='"+data.location+"'>");
                         

                    }else{
                        $("#location").html('Not Available');
                    }
            }
          });
        }
        </script> 

        @stack('custom')


</body>

</html>
