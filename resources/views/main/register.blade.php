@extends('main.layout.template_location')

@section('content')
	

	<div class="blogs" id="one">
		<div class="container" >
			@include('admin.shared.notif')
			<form class="form-horizontal" method="post" action="{{url('/save-register')}}">
                        {{csrf_field()}}
			
				<div class="row" style="padding-top: 90px !important; padding-bottom:  60px !important">
					
					<div class="col text-center" style="margin-bottom: 60px">
						<div class="section_title">
							<h2>{{trans('home/register.register')}}</h2>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-6">
						<div>
							 <h4>{{trans('home/register.name')}} :</h4>
							<input id="name_reg" type="text" class="form_input" name="name" placeholder="{{trans('home/register.name')}}" required="required" data-error="Nama sah diperlukan">
							@if ($errors->has('name'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('name') }}</strong>
                                </span>
                            @endif

                            <h4>{{trans('home/register.ic')}} :</h4>
							<input id="ic_reg" type="text" class="form_input" name="ic" placeholder="{{trans('home/register.ic')}}" required="required" data-error="No MyKad sah diperlukan">
							@if ($errors->has('ic'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('ic') }}</strong>
                                </span>
                            @endif

                            <h4>{{trans('home/register.phone')}}</h4>
							<input id="mobile_phone" type="text" class="form_input" name="mobile_phone" placeholder="{{trans('home/register.phone')}}" required="required" data-error="Nombor Telefon Bimbit sah diperlukan">
							@if ($errors->has('mobile_phone'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('mobile_phone') }}</strong>
                                </span>
                            @endif
						</div>
					</div>
					<div class="col-lg-6">
						<div>
							<h4>{{trans('home/register.email')}} :</h4>
							<input id="email" type="email" class="form_input" name="email" placeholder="{{trans('home/register.email')}}" required="required" data-error="Alamat E-mel sah diperlukan">

							@if ($errors->has('email'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                            @endif
                              <h4>{{trans('home/register.password')}}</h4>
							<input id="password" type="password" class="form_input" name="password" placeholder="{{trans('home/register.password')}}" required="required" data-error="Nombor Telefon Bimbit sah diperlukan">
							@if ($errors->has('password'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                            @endif

							
						</div>
					</div>
				</div>
                <input type="hidden" class="form-control" name="kodlokaliti_reg" required="" id="kodlokaliti_reg">
                <input type="hidden" class="form-control" name="NamaLokaliti_reg" required="" id="NamaLokaliti_reg">
                <input type="hidden" class="form-control" name="NamaDM_reg" required="" id="NamaDM_reg">
                <input type="hidden" class="form-control" name="NamaDUN_reg" required="" id="NamaDUN_reg">
                <input type="hidden" class="form-control" name="NamaParlimen_reg" required="" id="NamaParlimen_reg">
                <input type="hidden" class="form-control" name="Negeri_reg" required="" id="Negeri_reg">
                <input type='hidden' name='latitude' id='latitude' >
                <input type='hidden' name='longitude' id='longitude'>
                <input type='hidden' name='location' id='location' >
				<div class="row">
					<div class="col-lg-12">
						<div>
							<button id="review_submit" type="submit" class="newsletter_submit_btn trans_300" value="Submit">{{trans('home/register.register')}}</button>
						</div>
					</div>
				</div>
			</form>
		</div>
	</div>
@endsection

