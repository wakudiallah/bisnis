@extends('main.layout.template_contact')

@section('content')

	<div class="container contact_container">
		<div class="row">
			<div class="col">
				<!-- Breadcrumbs -->
				<div class="breadcrumbs d-flex flex-row align-items-center">
					<ul>
						<li><a href="{{url('/')}}">Home</a></li>
						<li class="active"><a href="#"><i class="fa fa-angle-right" aria-hidden="true"></i>{{trans('home/contact.contact')}}</a></li>
					</ul>
				</div>
			</div>
		</div>

		<!-- Map Container -->

		<div class="row">
			<div class="col">
				<div id="google_map">
					<div class="map_container">
						<div class="b-map wow zoomInUp" data-wow-delay="0.5s">
			<!--<script type="text/javascript" charset="utf-8" src="https://api-maps.yandex.ru/services/constructor/1.0/js/?sid=01.740665%2C3.196834&z&width=100%&height=400&lang=en_US&sourceType=constructor"></script>-->
			<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3983.6091372478813!2d101.73845681364985!3d3.196941153751227!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x31cc3825892b642b%3A0x5c9faf474a6853e8!2s22%2C+Jalan+Wangsa+Delima+10%2C+Wangsa+Maju%2C+53300+Kuala+Lumpur%2C+Wilayah+Persekutuan+Kuala+Lumpur%2C+Malaysia!5e0!3m2!1sen!2s!4v1508488940942" width="1600" height="400" frameborder="0" style="border:0" allowfullscreen></iframe>
		</div>
					</div>
				</div>
			</div>
		</div>

		<!-- Contact Us -->

		<div class="row">

			<div class="col-lg-6 contact_col">
				<div class="contact_contents">
					<h1>{{trans('home/contact.contact')}}</h1>
					<p>{{trans('home/contact.sentence')}}</p>
					<div>
						<p>(800) 686-6688</p>
						<p>info.deercreative@gmail.com</p>
					</div>
					<div>
						<p>mm</p>
					</div>
					<div>
						<p>{{trans('home/contact.weekday')}}</p>
						<p>{{trans('home/contact.weekend')}}</p>
					</div>
				</div>

				<!-- Follow Us -->

				<div class="follow_us_contents">
					<h1>Follow Us</h1>
					<ul class="social d-flex flex-row">
						<li><a href="#" style="background-color: #3a61c9"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
						<li><a href="#" style="background-color: #41a1f6"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
						<li><a href="#" style="background-color: #fb4343"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
						<li><a href="#" style="background-color: #8f6247"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
					</ul>
				</div>

			</div>

			<div class="col-lg-6 get_in_touch_col">
				<div class="get_in_touch_contents">
					<h1>{{trans('home/contact.get')}}</h1>
					<p>{{trans('home/contact.fill')}}</p>
					<form action="post">
						<div>
							<input id="input_name" class="form_input input_name input_ph" type="text" name="name" placeholder="{{trans('home/contact.name')}}" required="required" data-error="Name is required.">
							<input id="input_email" class="form_input input_email input_ph" type="email" name="email" placeholder="{{trans('home/contact.email')}}" required="required" data-error="Valid email is required.">
							<input id="input_website" class="form_input input_website input_ph" type="url" name="name" placeholder="Website" required="required" data-error="Name is required.">
							<textarea id="input_message" class="input_ph input_message" name="message"  placeholder="{{trans('home/contact.message')}}" rows="3" required data-error="Please, write us a message."></textarea>
						</div>
						<div>
							<button id="review_submit" type="submit" class="red_button message_submit_btn trans_300" value="Submit">{{trans('home/contact.send')}}</button>
						</div>
					</form>
				</div>
			</div>

		</div>
	</div>
@endsection		

