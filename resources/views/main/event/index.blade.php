@extends('main.layout.template_fix')

@section('content')
	<link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.1.0/fullcalendar.min.css' />
	



    <div class="container product_section_container">
        <div class="row" style="padding-top: 90px !important; padding-bottom:  60px !important">
            <div class="col product_section clearfix">

                <!-- Breadcrumbs -->

                <div class="col text-center" style="margin-bottom: 60px">
                    <div class="section_title">
                        <h2>{{trans('home/event.event')}}</h2>
                    </div>
                </div>

                <!-- Main Content -->

                <div class="main_content">

                    <!-- Products -->

                    <div class="products_iso">
                        <div class="row">
                            
                            <div class="col-lg-2"></div>
                            <div class="col-lg-8">
                                <div class="product_details">
                                     <div id='calendar'></div>
                                </div>
                            </div>
                            <div class="col-lg-2"></div>
       

                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>



@endsection

@push('custom')
<script src='https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.17.1/moment.min.js'></script>
<script src='https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.1.0/fullcalendar.min.js'></script>
<script>
    $(document).ready(function() {
        // page is now ready, initialize the calendar...
        $('#calendar').fullCalendar({
            // put your options and callbacks here
            events : [
                @foreach($event as $event)
                {
                    title : '{{ $event->title }}',
                    start : '{{ $event->event_date }}',
                    url : '{{url('/')}}/event/detail/{{$event->id}}'
                },
                @endforeach
            ]
        })
    });
</script>
@endpush
